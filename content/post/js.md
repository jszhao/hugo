---
title: 'js 正则替换一例'
date: 2013-05-02T23:21:00.001-07:00
draft: false
aliases: [ "/2013/05/js.html" ]
---

直接上例子```
  function marktext(text) {  
``````
      var re = new RegExp(key, "gi");  
``````
      return text.replace(re, "<mark>$&</mark>");  
``````
  },  
``````
  function marktext(text) {  
``````
      var re = new RegExp(key, "gi");  
``````
      return text.replace(re, function (word) { return "<mark>" + word + "</mark>" });  
``````
  },      这两个方法都是给指定不区分大小写的内容做mark标记的方法，功能一样，相比直接替换的优势就是不会改变被替换文字的大小写。第二个方法，使用起来更灵活。  
```  
  
https://byszsz.com/archives/526.htm?utm\_source=rss&utm\_medium=rss&utm\_campaign=js-%25e6%25ad%25a3%25e5%2588%2599%25e6%259b%25bf%25e6%258d%25a2%25e4%25b8%2580%25e4%25be%258b