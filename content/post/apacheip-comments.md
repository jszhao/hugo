---
title: 'Apache:基于名称的虚拟主机和基于IP的虚拟主机'
date: 2011-10-19T11:30:00.000-07:00
draft: false
aliases: [ "/2011/10/apacheip.html" ]
tags : [apache, php]
---

#### wamp5下的虚拟主机的配置  
1 编辑httpd.conf，查找Include conf/e...
[kivu23]( "noreply@blogger.com") - <time datetime="2011-10-31T03:15:47.000-07:00">Oct 1, 2011</time>

wamp5下的虚拟主机的配置  
1 编辑httpd.conf，查找Include conf/extra/httpd-vhosts.conf，把前面注释符号“#”删掉。  
2 编辑httpd-vhosts.conf，我把WAMPServer安装在D:/wamp，所以我这里的路径是D:wampApache2confextra。  
  
把里面的内容清空掉，换成下面的内容：  
  
NameVirtualHost \*:80  
  
ServerName www.host1.com  
ServerAlias www.host1.com  
DocumentRoot “D:/wamp/www/host1″  
  
  
ServerName www.host2.com  
ServerAlias www.host2.com  
DocumentRoot “D:/wamp/www/host2″  
  
  
3 编辑httpd.conf，找到DocumentRoot “d:/wamp/www/”这项，这是默认根目录路径，但是要更改的不是这个，一直往下找，找到，然后在该后加上如下内容：  
  
  
  
Options Indexes FollowSymLinks  
  
AllowOverride all  
Order Allow,Deny  
Allow from all  
  
  
  
Options Indexes FollowSymLinks  
AllowOverride all  
Order Allow,Deny  
Allow from all  
  
  
4 修改C:/WINDOWS/system3/drivers/etc/host这个文件，用记事本打开，加上如下内容:  
127.0.0.1 www.host1.com  
127.0.0.1 www.host2.com  
  
好了，然后重启apache，在浏览器里面输入www.host1.com，看看访问到的内容是不是host1这个目录呢。
<hr />
#### [@kivu23](#comment-16) ...
[Jason.Z]( "noreply@blogger.com") - <time datetime="2011-10-31T06:33:33.000-07:00">Oct 1, 2011</time>

[@kivu23](#comment-16)  
多谢@kivu23 提供wamp5下的配置方法！
<hr />
#### [@Jason.Z](#comment-17) ...
[kivu23]( "noreply@blogger.com") - <time datetime="2011-11-07T10:15:14.000-08:00">Nov 2, 2011</time>

[@Jason.Z](#comment-17)  
嘻嘻，我又回来了，关注你出新东西，学学习
<hr />
#### 多谢多谢，最近比较忙，写的东西少，我会尽量努力，争取多写一些好的东西出来。
[Jason.Z]( "noreply@blogger.com") - <time datetime="2011-11-07T10:18:14.000-08:00">Nov 2, 2011</time>

多谢多谢，最近比较忙，写的东西少，我会尽量努力，争取多写一些好的东西出来。
<hr />
