---
title: 'Facebook php sdk 使用入门'
date: 2011-10-20T11:13:00.000-07:00
draft: false
aliases: [ "/2011/10/facebook-php-sdk.html" ]
tags : [开放平台, api, facebook, php, web]
---

> 昨日有朋友问及关于facebook 的php sdk的问题，由于之前并没有关注其php sdk，并不是特别的了解，于是专门下载了php sdk来试用，现把学习所得与大家分享。

下载php sdk
---------

首先，到facebook的开发者站上下载php的sdk，具体页面在以下地址：

[https://developers.facebook.com/docs/reference/php/](https://developers.facebook.com/docs/reference/php/)

这个页面其实是使用文档，讲到了许多使用方法。下载后把下载的压缩包解压缩到站点的目录中即可。

appkey和secretkey
----------------

下载的sdk中，有个example.php的文件（具体在example的子文件夹中），这其实就是一个登录并获取用户基本信息的一个演示。另外还有一个结合jssdk的演示，功能一样，效果不一样而已。

演示中提供了默认的appkey和secretkey，但是我只是第一次测试的时候，用它登录成功了，但后来再测试的时候就不能正常使用了，换了我的某一个应用的key，还是不能访问，提示说没有通过url访问的权限之类的说法。不得已，只好创建了一个新的应用，设置url为测试用的域名，还是报一样的错误。

仔细查找后发现，facebook此次升级版本后，增加了好多设置，其中有个url访问的设置默认是关闭的，将其打开即可。具体该选项在高级设置（Advanced）里，叫做Stream post URL security。

演示代码分析
------

我们只分析php sdk的代码，关于jssdk的代码之前曾有文章提到过。

小提示：

> 使用php sdk，必须打开php\_curl.dll，在php.ini中，找到该行，去掉前面的分号，重启apache即可。

首先，引入facebook 的phpsdk，并初始化facebook对象：

```
require '../src/facebook.php';  
  
// Create our Application instance (replace this with your appId and secret).  
$facebook = new Facebook(array(  
  'appId'  => '191149314281714',  
  'secret' => '73b67bf1c825fa47efae70a46c18906b',  
));
```  
  

初始化中，要把appid后的数字修改为自己申请到的应用的appkey，secret也修改为对应的secretkey，注意secretkey是可变的，如果你觉得你的应用被其他人冒用，可以重新生成该值，但你所有的应用中对应值都要相应的修改。

  
  

查看是否有用户已登录：

  
  
```
$user = $facebook->getUser();  
  
// We may or may not have this data based on whether the user is logged in.  
//  
// If we have a $user id here, it means we know the user is logged into  
// Facebook, but we don't know if the access token is valid. An access  
// token is invalid if the user logged out of Facebook.  
  
if ($user) {  
  try {  
    // Proceed knowing you have a logged in user who's authenticated.  
    $user\_profile = $facebook->api('/me');  
  } catch (FacebookApiException $e) {  
    error\_log($e);  
    $user = null;  
  }  
}  
  
// Login or logout url will be needed depending on current user state.  
if ($user) {  
  $logoutUrl = $facebook->getLogoutUrl();  
} else {  
  $loginUrl = $facebook->getLoginUrl();  
}
```  
  

$user是通过sdk检测是否用户已经登录。

  
  

其中提到的几个方法分别说一下：

  
  

>   
> 
> $facebook->getUser();
> 
>   

  
  

如果已登录，$user就是用户id，否则为空。

  
  

>   
> 
> $facebook->getLogoutUrl();
> 
>   

  
  

如果用户已经登录，可以通过此方法，获取一个退出的链接。

  
  

$facebook->getLoginUrl(); 如果用户未登录，可以通过此方法，获取一个登录的链接。点击该链接会到一个登录页面并授权的页面，登录成功并确认授权后会返回之前的页面。

  
  

>   
> 
> $facebook->api('/me');
> 
>   

  
  

获取当前登录用户的详细信息；如果要访问某用户的信息，比如naitik'，则可以写做：

  
  

>   
> 
> $facebook->api('/naitik');
> 
>   

  
  

其中，$facebook->api('/me'); 就是调用具体api的写法。

  
  

前面的文章中提到过js sdk的使用，有这么一段：

  
  
```
FB.api({path}, 'GET', {}, function(response){  
    if (!response || response.error) {  
    alert('Error occured');  
  }  
    else {  
    …  
  }  
})
```  
  

其实这里的写法也是完全类似的，第一个参数就是路径，第二个参数是访问谓词，一般是GET、POST、DELETE等，第三个参数是一个数组，就是对应的方法的文档中所需要的参数。当然，我们这个程序就不需要回调函数了，呵呵，下面有一个完整的写法示例：

  
  
```
$facebook->api('/daaku.shah', 'DELETE', array(  
        'client\_id' => self::MIGRATED\_APP\_ID));
```  
  

  
  

总结
--

  
  

总的来看，facebook提供的sdk还是比较好用的。虽然在论坛上有许多人说它难用，但我估计是因为facebook太庞大了，api研究不过来，不像优库那样只有一个接口。对比严重抄袭facebook的renren，renren的api设计的简直是欠抽：那个东西才叫做难用，搞好几天都调不通！再一个，说说土豆，文档跟进缓慢，很多东西忘记写入文档了？？？而且示例也不通！

  
  

废话说多了，把国内的几个网站都得罪了，不过还是希望提到的几个网站能够及时优化，开发平台如果很难用，就别开放了。

  
  

好了，关于php sdk的入门就写到这里，感觉往下也没有什么好写的了（难道要写具体的api方法？）。如果朋友们有兴趣，欢迎留言，我们再深入探讨。