---
title: 'zend framework 备忘'
date: 2011-09-23T08:10:00.000-07:00
draft: false
aliases: [ "/2011/09/zend-framework.html" ]
tags : [zend framework, php]
---

> 最近工作比较乱，不想多说。研究一周多的php工作要暂停，不知道到时候恢复的时候是不是能想的起来。先记录下来一部分，做个备忘，免得将来又从头开始。
> 
> 环境配置不记录了，前面有提到。zend framework就是一组类库，下载下来放在站点根目录的library目录中即可，index.php做程序入口，所有访问经该入口路由到具体的处理。

一般开发步骤
------

根据当前的开发方式，实际开发时，每创建一个页面，有以下几个步骤：

#### 1、创建表对象

在application目录的models目录下，创建表的表对象，其实很简单，通过zend studio可以直接创建，代码如下：

> <?php  
> require\_once 'Zend/Db/Table/Abstract.php';  
> class feed extends Zend\_Db\_Table\_Abstract  
> {  
>     /\*\*  
>      \* The default table name  
>      \*/  
>     protected $\_name = 'feed';  
> }

这是一个简单的feed对象，对应数据库中的feed表，有了它以后，可以直接用面向对象的方式访问该表，而不需要编写sql语句。当然，也可以在里面封装些数据库的常用操作方法。数据操作的方法记录在后面。

#### 2、在application.ini中创建路由配置

比如

> routes.register.type = "Zend\_Controller\_Router\_Route\_Static"  
> routes.register.route = "user/register"  
> routes.register.defaults.controller = "register"  
> routes.register.defaults.action = "index"

这是一段注册页面的配置，注册类型是静态地址，地址为user/register，对应的控制器是register，控制器中的action是index。

再看另一段配置：

> routes.shot.route = "project/shot/list/:pid"  
> routes.shot.defaults.controller = "shot"  
> routes.shot.defaults.action = "index"  
> routes.shot.map.1="pid"

这里pid是参数，意为project/shot/list/xx 这样一个路径xx就是pid参数，可以用request方式获取到值。

#### 3、编写控制器

在我们的方法中，所有程序都放在了modules文件夹中，在modules/default/controller中创建控制器，比如我们路由中有个register的控制器，其名称就是：registerContrlller.php。

看下面这个代码：

> <?php  
> require\_once 'Zend/Controller/Action.php';  
> class shotController extends Zend\_Controller\_Action
> 
> {  
>     public function init ()  
>     {  
>         $this->\_helper->layout()->setLayout("default");  
>     }
> 
>    public function indexAction ()
> 
>    {…
> 
>    }
> 
> }

这个控制器是针对前面第二段路由配置的，类名是shotController，继承自

Custom\_Controllers\_AuthController（这个zendstudio会自动生成，不用担心记不住），indexAction就是该路由的触发的动作，一个控制器中可以有多个action。init方法是初始化方法，比如某控制器中有多个action，则每个action执行前都会执行init方法。

#### 4、制作模板

所谓模板，就是以phtml为后缀的一个html块，不过其中可以写php代码，示例：

> <div class="loginP">  
> <h2>New Shot</h2>  
> <?php  
> echo $this->form;
> 
> ?>
> 
> </div>  
> <hr />  
> <h2>Shot List</h2>  
> <table class="UserList">  
> <tr>  
> <td>shot</td><td>budget</td>  
> </tr>  
> <?php  
> if($this->shots!=null){  
> foreach ($this->shots as $u){  
>     ?>  
> <tr><td><?php echo $u->name ?></td><td><?php echo $u->budget ?></td><td><a href='/shot/delete/<?php echo  $u->id?>'>del</a></td><td><a href='/shot/delete/<?php echo  $u->id?>'>edit</a></td><td><a href='/shot/delete/<?php echo  $u->id?>'>close</a></td></tr>  
> <?php }}?>  
> </table>  

这就是一个简单的模板，$this->shots需要在控制器中进行赋值，该例中是个对象数组，这里有循环输出的示例，不多说了。

到这里一个带基本的显示功能的页面已经开发完了。看起来还是蛮简单的。

zend framework的数据库操作
--------------------

zend framework数据库访问还是比较方便的，比如在models中的，可以直接将表的数据读成对象数组，简单的操作方法有：

> $db = $this->getAdapter();  
> $where = $db->quoteInto('pid = ?', $pid);          
> $row = $this->fetchAll($where,'id desc');

fetchAll是一个返回对象数组的方法，可以遍历显示或处理。这段代码是个完整的用法。

下面这段代码执行添加操作：

> $row = $form->getValues();  
> $row\['createtime'\] = date('Y-m-d H:i:s', time());
> 
> $row\['creator'\] =$uid;   
> $row\['project'\] = $pid;   
> ...    
> $shot = new shot();  
> $sid = $shot->insert($row);

直接吧一个数组插入到表记录中。

删除更简单：

> $user = new users();  
> $rows = $user->deluser($userid);

如果要使用事务，也很简单

>         $db->beginTransaction();  
>         try {  
>             ...           
>             $db->commit();  
>         }  
>         catch (Exception $e){  
>             $db->rollBack();  
>         } 

好了，似乎也就这么多需要记录的了。