---
title: 'Webkit中textarea的设定'
date: 2012-03-04T02:09:00.000-08:00
draft: false
aliases: [ "/2012/03/webkittextarea.html" ]
tags : [webkit, web]
---

> 使用chrome浏览器或者safari浏览器，经常会发现自己的textarea很奇怪，可以拖动放大缩小，而且还有个奇怪的边。最初我们遇到这类问题的时候，直接给设计交代说那是浏览器的特性，俺们管不着，结果人家拿来google做例子，我们做开发的无话可说。但美工也不知道怎么做，只好开发人员硬着头皮去比较google的源码去研究。也没啥好方法，一个一个尝试呗。

其实找到以后也很简单，这里记录下，免得忘记：

**其一、页面输入框（input 标签）聚焦高亮。**﻿

![](http://www.uedbox.com/wp-content/uploads/2010/08/chrome3.png "chrome3")

屏蔽input聚焦高亮效果的样式：

```
input {outline: none;}  
textarea {outline: none;}  

```  

**其二、文本框（textarea 标签）缩放功能。  
![](http://www.uedbox.com/wp-content/uploads/2010/08/chrome2.png "chrome2")  
**

```
/\*css2.0\*/  
textarea {width: 400px;max-width: 400px;height: 400px;max-height: 400px;}  
/\*css3.0\*/  
texearea {resize: none;}
```  

可能将来的网页会偏向简洁设计，所以webkit开发团队已经不考虑当设计的输入框是圆角的时候输入框和文本框出现的问题，所以当你隐藏了点击前的样式而你如果忘记去掉了聚焦后的webkit赋予的默认样式，webkit引擎浏览器的就会出现问题。  

如图：![](http://www.uedbox.com/wp-content/uploads/2010/08/chrome.png "chrome")  

如果考虑兼容webkit核心的浏览器，建议设计输入框或者文本框的时候尽量保持原始的样式，如果设计做了很好看的圆角背影的效果，建议前端最好将下边的代码加到base.css里边去，避免用webkit浏览器看出现不同的效果。