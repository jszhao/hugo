---
title: 'CoffeeScript 安装配置笔记（Windows 8&Sublime Text2）'
date: 2013-06-28T01:36:00.019-07:00
draft: false
aliases: [ "/2013/06/coffeescript-windows-8-text2.html" ]
---

安装步骤其实还算简单：

1、安装node.js。

2、更新npm，命令行下执行：

```
  npm update  
``````
  3、使用npm安装Coffee Script:  
``````
  npm install \-g coffee-script  
```

如果网络有问题，可以去这里下载一个zip包使用：

```
  [https://github.com/jashkenas/coffee-script/](https://github.com/jashkenas/coffee-script/)  
```

关于zip包的使用方式，其实就是把下载下来的zip包解压到：

```
  C:\\Users\\username\\AppData\\Roaming\\npm\\node\_modules\\coffee-script\\  
```

目录中就可以了，另外有说需要添加环境变量的：

```
  NODE\_PATH\=C:\\Users\\username\\AppData\\Roaming\\npm\\node\_modules  
```

到现在为止，其实Coffee Script已经安装完成，你可以通过查看版本号的命令来检查安装时候成功。

```
  coffee –v  
```

然后我们就要开始配置Sublime Text2的环境了。Sublime Text2其实是提供了一个插件的，可以在用install命令直接安装。不过安装后并不好使（编译报错），需要修改插件配置, 在插件Sublime Text的安装目录：

```
  \\Data\\Packages\\CoffeeScript\\CoffeeScript.sublime-build  
```

打开该文件，使用如下代码覆盖：

```
  {  
``````
      "cmd": \["C:\\\\Program Files\\\\nodejs\\\\coffee.cmd", "$file","&&","C:\\\\Program Files\\\\nodejs\\\\coffee.cmd","\-c","$file"\],  
``````
      "file\_regex": "^(...\*?):(\[0-9\]\*):?(\[0-9\]\*)",  
``````
      "selector": "source.coffee"  
``````
  }  
```

可以看到这段代码跟我们网上查到的代码有些不一样，其实coffee命令直接跟$file的话，是控制台输出，中间夹一个”-c”的参数，就是编译了，编译就是把我们创建的.coffee文件编译成指定的.js文件。我们这里的配置的参数是同步录下同名的js文件。（如果文件已经存在，会覆盖）

当然，这里有个coffee.cmd,其实我看到这段代码时也很迷惑的，这段coffee.cmd的作用是什么呢？因为nodejs里面并没有这个命令啊。

经过一番查找，命令也很简单：

```
  @echo off  
``````
  "C:\\Program Files\\nodejs\\node.exe" "C:\\Program Files\\nodejs\\node\_modules\\npm\\node\_modules\\coffee-script\\bin\\coffee" %\*  
```

好了，把这个命令保存成coffee.cmd，放在nodejs的根目录下就可以了。

好了，现在就使用sublime text来编写coffee script文件了。

如果你还不太熟悉，这里有个小提示：

1.  sublime text编译快捷键是CTRL+B。
2.  编写coffeescript 的代码，后缀名是.coffee
3.  编译后，生成同名js文件。可以使用拆分窗口的方式，方便对比生成的js代码，学习时尤其有效。

参考链接：

[http://www.ibm.com/developerworks/cn/web/wa-coffee1/](http://www.ibm.com/developerworks/cn/web/wa-coffee1/)

[http://www.cnblogs.com/2gua/archive/2012/07/04/2576352.html](http://www.cnblogs.com/2gua/archive/2012/07/04/2576352.html)

[http://www.ituring.com.cn/article/4048](http://www.ituring.com.cn/article/4048)

[http://rritw.com/a/bianchengyuyan/C\_\_/20121026/242739.html](http://rritw.com/a/bianchengyuyan/C__/20121026/242739.html)

**原创文章，转载请注明：** 转载自[Happiness space](https://byszsz.com/)

**本文链接地址:** [CoffeeScript 安装配置笔记（Windows 8&Sublime Text2）](https://byszsz.com/archives/557.htm)

  
  
[CoffeeScript 安装配置笔记（Windows 8&Sublime Text2）](https://byszsz.com/archives/557.htm?utm_source=rss&utm_medium=rss&utm_campaign=coffeescript-%25e5%25ae%2589%25e8%25a3%2585%25e9%2585%258d%25e7%25bd%25ae%25e7%25ac%2594%25e8%25ae%25b0%25ef%25bc%2588windows-8sublime-text2%25ef%25bc%2589)