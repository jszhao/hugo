---
title: '判断网络是否连接（win8 app HTML&JS）'
date: 2013-06-28T01:36:00.011-07:00
draft: false
aliases: [ "/2013/06/win8-app-html.html" ]
---

```
  var networkInfo = Windows.Networking.Connectivity.NetworkInformation;  
```

```
  //Retrieve the ConnectionProfile  
```

```
  var internetConnectionProfile = networkInfo.getInternetConnectionProfile();  
```

```
  //Pass the returned object to a function that accesses the connection data    
```

```
  var connectionProfileInfo = getConnectionProfileInfo(internetConnectionProfile);  
```

**原创文章，转载请注明：** 转载自[Happiness space](https://byszsz.com/)

**本文链接地址:** [判断网络是否连接（win8 app HTML&JS）](https://byszsz.com/archives/523.htm)

```
      
  
[判断网络是否连接（win8 app HTML&JS）](https://byszsz.com/archives/523.htm?utm_source=rss&utm_medium=rss&utm_campaign=%25e5%2588%25a4%25e6%2596%25ad%25e7%25bd%2591%25e7%25bb%259c%25e6%2598%25af%25e5%2590%25a6%25e8%25bf%259e%25e6%258e%25a5%25ef%25bc%2588win8-app-htmljs%25ef%25bc%2589)  
```