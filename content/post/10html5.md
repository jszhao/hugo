---
title: '推荐10款不错的HTML5框架'
date: 2011-11-09T00:38:00.000-08:00
draft: false
aliases: [ "/2011/11/10html5.html" ]
tags : [资源, HTML5, web]
---

> 前两个月还在感叹WebOS的垮掉，现在突然发现HTML5比预期的时间提前来到了。难道真的是惠普的WebOS就是死在早了那么一两年么？那么真如现在所说的Oracle要低价收购的话，那真是在最合适的时间捡到一个大便宜啊。好了，废话不多说，今天推荐10款不错的HTML5的开发框架。

  

1、 Sencha Touch
---------------

  
Sencha Touch是专门为移动设备开发应用的Javascrt框架。通过Sencha Touch你可以创建非常像native app的web app，用户界面组件和数据管理全部基于HTML5和CSS3的web标准，全面兼容Android和Apple iOS。这个是大名鼎鼎的EXT框架的公司整合其另外两个框架发展而来的，自然首先推荐。  
  
[![17183957_tLNm[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/17183957_tLNm1_thumb.png "17183957_tLNm[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/17183957_tLNm1.png)[![17183959_FanS[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/17183959_FanS1_thumb.png "17183959_FanS[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/17183959_FanS1.png)  
  
今天 Sencha Touch 发布 2.0 的首个开发者预览版。这在 Sencha Touch 1.0 发布的一年之后。新版本主要工作侧重于性能提升、易用以及内置打包方面。  
  
相关连接：  
  
[下载 Touch](http://cdn.sencha.io/touch/sencha-touch-2.0.0-pr1.zip)[  
Touch 文档](http://docs.sencha.com/touch/2-0/)  
[下载 SDK 工具 (Mac-only)](http://cdn.sencha.io/sdk-tools/SenchaSDKTools-2.0.0-Developer-Preview.tar.gz)  
  
下图是 ST 2.0 和 1.0 在启动时间的比较：  
  
[![12072727_b3Di[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/12072727_b3Di1_thumb.png "12072727_b3Di[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/12072727_b3Di1.png)  
  
另外 2.0 版本在文档方面也做了很多改进，[查看 2.0 文档](http://docs.sencha.com/touch/2-0/)。  

2、PhoneGap
----------

  
PhoneGap是一款HTML5平台，通过它，开发商可以使用HTML、CSS及JavaScript来开发本地移动应用程序。因此，目前开发商可以只 编写一次应用程序，然后在6个主要的移动平台和应用程序商店(app store)里进行发布，这些移动平台和应用程序商店包括：iOS、Android、BlackBerry、webOS、bada以及Symbian。上个月该公司已经被Adobe公司协议收购，有了强大的后台支撑。（话说Adobe也通过收购在HTML5开发标准上占了一席之地啊）  
  
[![12103344_JTVe[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/12103344_JTVe1_thumb.png "12103344_JTVe[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/12103344_JTVe1.png)  
  
下面是一些使用该框架的代码示例：  
  
获取地理位置：  
```
 //GAP will invoke this function once it has the location function gotLocation(lat,lon){ $('lat').innerHTML = "latitude: " + lat; $('lon').innerHTML = "longitude: " + lon; }
```  
获取摄像头捕捉的照片  
```
 function takePhoto(){ var photo = gap:takePhoto(); return photo; }
```  
[http://phonegap.com/](http://phonegap.com/)  
  
honeGap Mobile App XDK，可以让开发人员创建、模拟和测试[PhoneGap](http://www.oschina.net/p/phonegap)项目。该工具是一个集成开发环境（IDE），提供了用于创建HTML5和PhoneGap应用的全套开发工具。AppMobi称，新的XDK为PhoneGap项目开发提供了一个直观的开发环境。  
  
[![17220652_sPDY[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/17220652_sPDY1_thumb.jpg "17220652_sPDY[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/17220652_sPDY1.jpg)  
  
备注： phoneGap已出现就有很大反响，现在国内有个[http://phonegap.cn](http://phonegap.cn/)爱好者网站，里面有中文的使用介绍。  

3、ChocolateChip-UI
------------------

  
ChocolateChip-UI 是一个手机移动 Web 开发框架，使用 HTML5, WAML, CSS 和 JavaScript，基于 ChocolateChip JavaScript 库，包含新的 ChUI.JS 和 ChUI.css.可以在没有什么设计的情况下，开发出类似原生应用的界面来。  
  
[![28070855_BOVI[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070855_BOVI1_thumb.png "28070855_BOVI[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070855_BOVI1.png) [![28070859_vAzZ[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070859_vAzZ1_thumb.png "28070859_vAzZ[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070859_vAzZ1.png)[![28070901_yBw0[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070901_yBw01_thumb.png "28070901_yBw0[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070901_yBw01.png) [![28070901_CJk1[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070901_CJk11_thumb.png "28070901_CJk1[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070901_CJk11.png) [![28070902_Y0Wp[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070902_Y0Wp1_thumb.png "28070902_Y0Wp[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070902_Y0Wp1.png) [![28070902_xU47[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070902_xU471_thumb.png "28070902_xU47[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070902_xU471.png) [![28070903_rJFR[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070903_rJFR1_thumb.png "28070903_rJFR[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070903_rJFR1.png) [![28070903_uOoe[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070903_uOoe1_thumb.png "28070903_uOoe[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070903_uOoe1.png) [![28070904_AEEz[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070904_AEEz1_thumb.png "28070904_AEEz[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070904_AEEz1.png) [![28070904_vwci[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070904_vwci1_thumb.png "28070904_vwci[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070904_vwci1.png) [![28070905_nH2O[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070905_nH2O1_thumb.png "28070905_nH2O[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070905_nH2O1.png) [![28070906_WMGK[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070906_WMGK1_thumb.png "28070906_WMGK[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070906_WMGK1.png) [![28070910_CEVh[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070910_CEVh1_thumb.png "28070910_CEVh[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070910_CEVh1.png) [![28070911_wE6f[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070911_wE6f1_thumb.png "28070911_wE6f[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070911_wE6f1.png) [![28070912_HWHN[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070912_HWHN1_thumb.png "28070912_HWHN[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070912_HWHN1.png)  

4、 Joshfire
-----------

  
Joshfire是一个支持多种设备的开发框架，仅采用HTML5和JavaScript等标准，可以帮助开发者迅速开发本地专用的网络应用，用于浏览器、Node.JS、桌面电脑、智能手机、智能电视以及联网设备。  

#### 多设备支持

  
[![28070205_emzX[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070205_emzX1_thumb.png "28070205_emzX[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070205_emzX1.png)  
  
不管你针对哪种设备开发，Joshfire可以让你「一次开发，多处兼容」，自动兼容手机、平板电脑、电视等设备。  

#### 针对内容进行优化

  
[![28070205_acYZ[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070205_acYZ1_thumb.png "28070205_acYZ[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070205_acYZ1.png)  
  
如果你想发布新闻、图片、音乐、视频，Joshfire框架可以帮你轻松开发全功能内容应用。  

#### 用户互动

  
[![28070206_haFW[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070206_haFW1_thumb.png "28070206_haFW[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/28070206_haFW1.png)  
  
忘掉那些为触控板、电视遥控器、键盘、鼠标、Kinect准备的专门设计吧，你只需要描述你的应用，其他的交给Joshfire。  

#### 客户端、服务器任意切换

  
下载地址：[http://framework.joshfire.com/download](http://framework.joshfire.com/download)  

5、DHTMLX Touch
--------------

  
它是针对移动和触摸设备的JavaScript 框架。DHTMLX Touch基于HTML5，创建移动web应用。它不只是一组UI 小工具，而是一个完整的框架，可以针对移动和触摸设备创建跨平台的web应用。它兼容主流的web浏览器，用DHTMLX Touch创建的应用，可以在iPad、iPhone、Android智能手机等上面运行流畅。  
  
[![21075436_uEmn[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/21075436_uEmn1_thumb.gif "21075436_uEmn[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/21075436_uEmn1.gif)  

6、jo
----

  
[![22133154_ywXC[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/22133154_ywXC1_thumb.png "22133154_ywXC[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/22133154_ywXC1.png)  
  
Jo这个框架可用于开发那支持HTML5的移动设备，如iOS,webOS, Android和Chrome OS等平台。  
  
它拥有标准，类原生的UI元素比如用于屏幕登录的Web表单控件，还有弹出小部件可用于在用户点击界面时提供一些额外的信息。  
  
可以查看其网站提供的[示例页面](http://joapp.com/demos.html)，它例子展示了在多种移动设备平上的Jo应用情况。  
  
[http://joapp.com/downloads.html](http://joapp.com/downloads.html)  

7、mobl
------

  
mobl 其实是一个新的开源的编程语言，主要用于加速手机应用的开发，mobl 可方便构建手机 web 应用程序，包括 iOS、Android 和其他支持 HTML5 技术的手机。  
  
[![15040939_S7CH[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/15040939_S7CH1_thumb.png "15040939_S7CH[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/15040939_S7CH1.png)  
  
主要特点：  

  
*   _Statically typed_ language, enabling great IDE support and as-you-type error detection, while reducing the amount of typing other statically typed languages require, through type inference.
  
*   Scripting language syntax similar to Javascript.
  
*   Declarative domain-specific language (DSL) for defining user interfaces.
  
*   Declarative concise DSL for defining data models. Data is _stored on the device_. No server back-end required.
  
*   Easy access to existing “native” Javascript libraries and widgets.
  
*   Compiler generates static Javascript and HTML files, ready to be deployed to any web host and to be cached on the device, to enable _offline web applications_.
  

  
[http://www.mobl-lang.org/get/](http://www.mobl-lang.org/get/)  

8、SproutCore
------------

  
SproutCore是一款HTML5的应用框架  
  
[在线演示](http://demo.sproutcore.com/)  

9、Perkins
---------

  
Perkins 是一个 HTML5/CSS3 框架，主要为简化开发者和设计师的工具。使用一个基础的 HTML5 模板，包含多数所支持的标签以及一些 CSS 样式，便于创建诸如导航、圆角、渐进等效果。遵循MIT协议。  
  
[![17070503_bmzn[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/17070503_bmzn1_thumb.jpg "17070503_bmzn[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/17070503_bmzn1.jpg)  
  
[http://code.google.com/p/perkins-less/](http://code.google.com/p/perkins-less/)  

10、AppMobi公司的HTML5框架
--------------------

  
一家名叫AppMobi的小公司，正致力一项工作，[使开发者能创建可深入移动设备硬件或操作系统功能的HTML5应用](http://www.infoworld.com/d/mobile-technology/have-your-html5-and-native-app-too-177559)， 如重力感应、加速计、GPS、相机、声音与震动、文件系统等，InfoWorld报道。‘其iOS MobiUs浏览器为游戏实现了HTML5的DirectCanvas API，并为将浏览器缓存中的可执行文件和数据存储在本地（以使应用可以离线运行）实现了HTML5本地存储API。但是使MobiUs不同于仅仅只是另 外一个浏览器的是，AppMobi在它上面提供了一个库，它能允许开发者访问本地硬件，并实现Web应用的消息推送。’  
  
[![04123557_lSXX[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/04123557_lSXX1_thumb.png "04123557_lSXX[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/11/04123557_lSXX1.png)  
  
网站  
  
[http://www.appmobi.com/](http://www.appmobi.com/)  
  
其开发中心链接  
  
[http://www.appmobi.com/?q=node/27](http://www.appmobi.com/?q=node/27)  
  
API 文档  
  
[http://appmobi.com/documentation/index.html](http://appmobi.com/documentation/index.html)  

> 这里罗列了这么多的HTML5的应用框架，大多数都没有尝试使用，不过多数的文档写的还算详细。而HTML5大多是冲着移动应用开发来的。其实在Web上的实现与在原生应用里面的效果如果一致，岂不是最受开发者和公司欢迎么？而对最终用户，才不管你使用什么技术、什么平台，只要效果好就可以啊。