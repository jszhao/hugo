---
title: '分享一个免费的web存储空间min.us（已挂）'
date: 2012-03-17T09:40:00.000-07:00
draft: false
aliases: [ "/2012/03/webminus.html" ]
tags : [资源]
---

> [min.us](http://min.us/rvyhf2r)是一个非常不错的在线存储空间，有需要的可以去申请下。

[min.us](http://min.us/rvyhf2r)是美国一家在线文件存储提供商，直接注册提供10G的在线存储空间，比dropbox等要大很多，最吸引我的一点是其支持直接外链，很是方便。主要指标为：

> 注册提供10G空间。  
> 可以通过邀请好友提升空间到50G，1G/好友。比dropbox的每好友250M强多了。  
> 提供直接外链。  
> 外链流量无限制。  
> 最大单文件大小限制：空间总容量的20%，10G的空间，大概有200M

邀请链接：[min.us](http://min.us/rvyhf2r)