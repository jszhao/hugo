---
title: '为网站使用免费的cdn：cloudflare'
date: 2012-03-11T03:37:00.000-07:00
draft: false
aliases: [ "/2012/03/cdncloudflare.html" ]
tags : [cdn, web]
---

> 本站点由于没有收入，只好使用免费的web空间000webhost，但免费的东西一定有各种各样的问题，前些天某个下午我自己访问站点的时候，竟然打不开网站，没有办法，向000webhost发邮件求助，不过我估计人家也爱答不理的，反正最后邮件也没回，但晚上看的时候，已经好了。不过由此开始，我就一直在想别的办法。

  
开始的时候，000webhost的ping值在300-400之间，而我的站点的ping值在500-600之间，着实无奈，直到发现有一个免费的cdn产品：CloudFlare。  
  
CDN，全称Content Delivery Networks，即内容分发网络。其原理很简单，即用户访问时内容不是从原始服务器上获得的，而是从CDN智能解析的服务器上获取的，内容是缓存到CDN服务器上的。通常CDN服务商会有多个机房，而智能解析总是到访问最快的机房，所以通过CDN能显著地加快访问速度。另外还有一个好处是CDN加速可以减轻服务器的流量压力。  
  
但是CDN也不是没有缺点，对于动态网页，由于有缓存，所以会造成延迟，很可能获取的的页面不是最新的，或者一些带有query的页面无法执行。但是对于静态内容效果还是十分显著的。  
  
人家别人拿CDN是用来缓存静态内容的，我的网站要解决的问题却是整个网站的访问问题，也就是说，当000webhost临时出现故障的时候，我的站点还可以正常访问，抗几分钟算几分钟吧。当然，为此带来的短时间上的不同步，还是可以承受的。  
  
本着试一试的态度，在[CloudFlare](https://www.cloudflare.com)上注册了一个帐号（注册使用的教程网上很多，这里就不写了），在dns上设置了nameserver的地址，就一直没转过去，后来发现从godaddy的dns管理中的的nameserver中添加[CloudFlare](https://www.cloudflare.com)的dnsserver是无效的，要在nameserver setting中做专门的设置，如图：  
  
[![nameserversetting](https://byszsz.com/wp-content/uploads/2012/03/nameserversetting_thumb.png "nameserversetting")](https://byszsz.com/wp-content/uploads/2012/03/nameserversetting.png)  
  
不过这个设置要24小时生效。  
  
第二天，收到了CloudFlare的邮件，说设置已经成功了，马上登陆上去看了下，似乎快了点（感觉不明显）。然后ping下，效果如图：  
  
[![ping_by_cloudflare](https://byszsz.com/wp-content/uploads/2012/03/ping_by_cloudflare_thumb.png "ping_by_cloudflare")](https://byszsz.com/wp-content/uploads/2012/03/ping_by_cloudflare.png)  
  
这个ping值还是挺让人欣慰的啊。  
  
不过使用它了之后，你修改了页面的样式或者添加了新的插件，是不能直接看到效果的，差不多要等个5分钟到10分钟吧。  
  
最后呢，我还有个问题，某天我自己打开网站的时候，网站直接被转入一个什么安全的网站(man-hosting.com)，说我的ip被两次发现有垃圾评论，不过输入一个什么验证码之后就没事了。我比较担心这个功能把许多来自中国的ip都给挡住了，而且本人也没有实地测试，不知道哪位大神能给予指点。