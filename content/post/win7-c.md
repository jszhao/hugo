---
title: 'win7 C盘清理'
date: 2011-09-06T20:49:00.000-07:00
draft: false
aliases: [ "/2011/09/win7-c.html" ]
tags : [资源]
---

> 这段时间，工作比较乱，各种开发环境的搭建、尝试多个开发工具，Win7的C盘hold不住了，本来我觉得50G的C盘不算太小，但某天却惊奇的发现C盘剩余空间是0！当然不久就变成100多兆了，用360清理助手也只能清理出300多兆（我平常经常做清理，垃圾本来就不多的）。但是想想我的软件都装到D盘去了啊，C盘只安装了那些必须安装到C盘的东西，什么visual studio、sqlserver还有windows phone开发包什么的，这些装别的盘符麻烦，而且本来50G的空间我觉得并不小。所以我决定手动清理下C盘。

> 清理之前，我们先看看下面这个搞笑图片：![](http://thumbsnap.com/i/i3h2lW8s.jpg)![](http://thumbsnap.com/i/OBVA1mWH.jpg)

                                           盖茨的磁盘空间理论

> 卸载不必要的程序
> --------
> 
> 首先，把该卸载的卸载掉。虽然大多数软件没有安装在C盘，但VS的插件似乎必须的装到C盘，那个intel C ++的编译器，再加上他的ipp库，足足占了2个G的空间，还有一些vs内置的插件，比如水晶报表啥的，不需要用的都卸载掉，当然这个是特例。
> 
> 清理用户文件夹
> -------
> 
> 第二个需要清除的文件夹是用户文件夹：
> 
> C:UsersuserAppData
> 
> users是电脑上当前用户的文件夹，电脑如果自己用，就不要建那么多用户了，每个用户都需要占一部分的空间，不常用的用户文件夹，直接删除即可，当然一般也没有多大空间，我们这里要清除的是最常用的用户的appdata文件夹。
> 
> 其实这个文件夹是比较有用的，如果整个删除的话，可能会引起部分程序无法使用。
> 
> 这个文件夹中又分为三个文件夹，其中Roamin文件夹和local文件夹占据了大量的空间（各占1G左右）。仔细分析其中的文件夹后发现，占据空间最大的分别是微软、苹果和谷歌。要命的是，google的谷歌浏览器程序文件竟然在这里，更新文件也不会自动删除，浏览器缓存高达800M。
> 
> 谷歌浏览器缓存路径默认为：
> 
> C:UsersuserAppDataLocalGoogleChromeUser DataDefaultCache
> 
> 建议直接删除。
> 
> 还有微软的文件夹中，带了很多不常用的程序，如果不需要，就删掉吧，路径：
> 
> C:UsersuserAppDataLocalMicrosoft
> 
> 另外，如果你使用Windows Live Mail，最好把默认的邮件保存路径改到其他路径，否则该文件夹也将是容量杀手。
> 
> 清理windows文件夹
> ------------
> 
> 前面讲的清理路径，都不是特别大量的文件夹，但windows目录占用了20G的空间，能不能清理下该目录呢？
> 
> 百度上找到一个清理目录，但所述的文件都不大，而且实际去删除的时候报没有权限（我没用的administrator），就放弃了。
> 
> ### 系统更新目录
> 
> 记得早在win2003时，windows更新目录会占很大的空间，可以直接删除：
> 
> C:WindowsDownloaded Installations
> 
> 现在win7中这个目录仍然存在，但我这里是空的，不知道是不是360帮忙清除掉了，该文件夹可以安全删除。
> 
> C:WindowsDownloaded Program Files
> 
> 这个是ie上安装的插件什么的目录，不需要的插件可以直接删，不过都不大。
> 
> ### 安装目录
> 
> 查看C盘中有个隐藏目录
> 
> C:WindowsInstaller
> 
> 该目录下竟然有10多G的占用，这个目录应该是安装程序是保留的临时文件。记得之前的windows安装程序的卸载直接放在程序目录里面，而且也不大，但这个目录里面放的，全是很大而且改名的msi或msp文件，能不能直接删呢？
> 
> 经查，大多数的说法是该目录的文件如果删除，程序就无法卸载，也有人说删除后一部分功能不能使用了，比如office会彻底不能用。
> 
> 其中有个$PatchCache$文件夹，看到有人评论说该目录可以完全删除（也有人说不可以删的），相信这句话是有风险的，但我还是信了。我删掉该目录（回收站），然后重启了电脑，打开office等常用的程序的，发现还能用，现在可以确信了。
> 
> 这就是说：
> 
> C:WindowsInstaller$PatchCache$
> 
> 目录是可以完全删除的，当时我的这个目录清出来6G的空间。
> 
> 对于Installer下面的msi文件，据说有个工具可以帮忙清理，下面有段来自网上的摘录：
> 
> > 在命令行，通过命令**msizap.exe G**可以删除一些孤儿文件：
> > 
> > C:>msizap.exe G  
> > MsiZapInfo: Performing operations for user S-1-5-21-1935655697-1409082233-839522115-1003  
> > Removing orphaned cached files.  
> > Removed file: C:WINDOWSInstaller102db95.mst  
> > Removed file: C:WINDOWSInstaller102dba2.mst  
> > Removed file: C:WINDOWSInstaller102dbac.mst  
> > Removed file: C:WINDOWSInstaller337c229.mst  
> > Removed file: C:WINDOWSInstaller3b1ae.mst  
> > Removed file: C:WINDOWSInstaller41485c5.mst  
> > Removed file: C:WINDOWSInstaller65a2f.mst  
> > Removed file: C:WINDOWSInstaller76bf7.mst  
> > Removed file: C:WINDOWSInstaller9419741.mst  
> > Removed file: C:WINDOWSInstaller9419770.mst  
> > Removed file: C:WINDOWSInstaller9419778.mst  
> > Removed file: C:WINDOWSInstallera43b7be.mst  
> > Removed file: C:WINDOWSInstallerf8fe2f1.mst
> > 
> > 网上还可以找到这样一个工具：  
> > **Windows Installer UnUsed Files Cleanup Tool （简称WICleanup）是一个用于清理 Windows Installer 冗余文件的工具。**  
> > 软件主页为：[http://www.kztechs.com/wicleanup/](http://www.kztechs.com/wicleanup/)  
> > 下载地址：[http://www.kztechs.com/wicleanup/wicleanup.zip](http://www.kztechs.com/wicleanup/wicleanup.zip)
> 
> 我试了，确实能删除，删掉几个msi和mst文件，但问题是删掉的都是小文件。仔细看这段中的孤儿文件，估计只是检查某些程序已经卸载但没有删除掉的安装文件，所以不解决大问题，还得靠自己手工。
> 
> 把详情打开，看看哪些文件永远都不需要删除的，自己删掉吧，比如vs的几个更新，有300兆的，有500兆的，挑大个的删。
> 
> 总结
> --
> 
> 经此次清理，C盘空间达到12G，又可以折腾一阵子了。其实之前也做过多次清理，但从未触及Installer文件夹，此次也算有所发现吧。