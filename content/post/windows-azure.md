---
title: '在windows azure中使用安全链接笔记'
date: 2012-01-20T03:47:00.000-08:00
draft: false
aliases: [ "/2012/01/windows-azure.html" ]
tags : [windows azure, 资源]
---

> 这里简单记录一下过程，其实大多数的说明在网上都有详细的说明，这里只详细记录遇到的过程。

1、在IIS中创建证书申请，注意一般加密的位长要设置为2048位。写入一个txt文件，可以生成一段密文，类似下面这段：

\-----BEGIN NEW CERTIFICATE REQUEST-----

.......

\-----END NEW CERTIFICATE REQUEST-----

2、把生成的证书文件提交给认证厂商。

3、对方会返回一个加密认证后的文本，将该文本保存为.cer文件，导入到iis中（使用完成证书申请）。

4、windows azure中的源代码中，找到Roles文件夹，选中.WebPortal文件，右键属性中，找到Certificates，点击Add Certificate

这是后会增加一行，前面的都不要管，只是在Thumbprint项中，点击后面的选择，会自动找到本机安装的所有证书，选择该证书即可。

5、在iis中将该证书导入到windows azure中的对应的证书目录中。然后上传编译好的代码。

注意事项，申请证书的计算机与完成认证过程的计算机需要是同一台，而且，申请和完成期间最好计算机不要做更新。我的电脑恰巧在第二天早上做了关于证书方面的更新，所以发生了证书完成后无效的问题，也就是说密钥被更新给弄乱了（具体情况不了解，反正是不能用了）。

解决办法就是找到证书的指纹，然后运行命令：

certutil -repairstore my "THUMBPRINT\_OF\_CERTIFICATE"

其中THUMBPRINT\_OF\_CERTIFICATE要替换成对应的指纹。

然后就可以了。

要回家了，先发这些吧，有时间再整理。