---
title: 'Notice: Trying to get property of non-object problem(PHP)解决办法'
date: 2012-03-05T11:18:00.000-08:00
draft: false
aliases: [ "/2012/03/notice-trying-to-get-property-of-non.html" ]
tags : [php]
---

> 今天又一次遇到PHP 的一个提醒:Notice: Trying to get property of non-object problem，这种错误很常见。上次出错，没查到原因，就简单的以这是一个Notice为由，没有做处理，今天有点时间，就查了查。

我这里实际是调用了一个zend的数据库访问的方法，使用了fetchAll方法，但由于数据库中没有该记录，所以返回的对象是null，所以我就判断对象是否为null:

```
if($obj==null){  
	...  
}  

```  

这么写的结果，就是产生了上面那个notice，也真是奇怪，对象为null，竟然不能访问了？

  

翻查资料后，发现，判断是否为null，需要这么判断：

```
if (isset($obj)) {  
	echo "This var is set set so I will print.";  
}
```  

这个isset是做什么的呢？

  

isset函数是检测变量是否设置。  

格式：bool isset ( mixed var \[, mixed var \[, ...\]\] )  

返回值：  

  
2.  若变量不存在则返回 FALSE  
    
3.  若变量存在且其值为NULL，也返回 FALSE  
    
4.  若变量存在且值不为NULL，则返回 TURE  
    
5.  同时检查多个变量时，每个单项都符合上一条要求时才返回 TRUE，否则结果为 FALSE

  

如果已经使用 unset() 释放了一个变量之后，它将不再是 isset()。若使用 isset() 测试一个被设置成 NULL 的变量，将返回 FALSE。同时要注意的是一个 NULL 字节（"\\0"）并不等同于 PHP 的 NULL 常数。  

警告: isset() 只能用于变量，因为传递任何其它参数都将造成解析错误。若想检测常量是否已设置，可使用 defined() 函数。  

看来刚才我那边的判断所出的问题，就是因为这个“是一个 NULL 字节（"\\0"）并不等同于 PHP 的 NULL 常数”。