---
title: 'youtubeAPI 及js调用示例'
date: 2011-12-12T10:29:00.000-08:00
draft: false
aliases: [ "/2011/12/youtubeapi-js.html" ]
tags : [开放平台, 资源, api]
---

> 本文记录youtube 的几个常用api，并使用其中一个介绍其使用方式。

  
首先，我们来看看这几个api：  
```
http://gdata.youtube.com/feeds/users/username/uploads  
\- videos uploaded by username  
  
http://gdata.youtube.com/feeds/users/username/favorites  
\- videos bookmarked by username  
  
http://gdata.youtube.com/feeds/users/username/playlists  
\- playlists created by username  
  
http://gdata.youtube.com/feeds/users/username/subscriptions  
\- username's subscriptions
```  
就是说，如果你要显示username这个用户（不知道有没有这个用户）的视频，可以访问  
[http://gdata.youtube.com/feeds/users/username/uploads](http://gdata.youtube.com/feeds/users/username/uploads)  
这个路径来获取数据。其他的就不用讲了。  
有趣的是，google给这四个api设置了差不多的参数，常用的有：  
?max-results=50：  
  
最多要返回的记录数(默认情况下是25)，不过要记住，youtube每次访问可返回的最大值恰好也是50，如果想要获得更多内容，可以多请求几次，哈哈。  
  
?alt=rss or ?alt=json：  
  
alt参数用于设置返回内容的格式。  
  
?vq=query：  
  
用于筛选结果，会从 metadata (title, tags, description)中进行检索，比如“google+ maps”，就是搜索google map。  
  
?orderby={updated, viewCount, rating, relevance}：  
  
排序，示例中以updated为第一排序，relevanced作为最后一个排序。  
  
使用jquery的AJAX进行访问也很简单：  
```
$.ajax({  
        type: "GET",  
        url: "http://gdata.youtube.com/feeds/users/username/uploads?alt=json”,  
        cache: false,  
        dataType: 'jsonp',  
        success: function (data) {  
            showMyVideos(data);  
        },  
       error: function (XMLHttpRequest, textStatus, errorThrown, data) {  
            alert("Not able to fetch the data due to feed unavailability!!!");  
       }  
});
```  
这里的showMyVideos(data)的方法就没必要写了，实际上我们只要随便找之前用到的播放器把内容显示出来就行了。而data结构还是比较复杂的，这里简单说一下。  
  
data实际是个数组，针对其中的每一个entry（=entries\[i\]），我们常用的内容有：  
  
entry.title.$t：视频的标题  
  
entries\[i\].media$group.media$thumbnail\[0\].url：视频的缩略图  
  
entries\[i\].media$group.media$content\[0\].url：视频的实际地址，这里content\[0\]实际是第一视频的地址，这个是flv格式的视频，如果不支持flash，可以取第二个。  
  
entry.published.$t：这是发布时间。  
  
entry.yt$statistics.viewCount：这个是被浏览次数。  
  
似乎没有直接提供视频的id，但是我们可以通过字符串算出来：  
  
entries\[i\].media$group.media$content\[0\].url.substr(playerUrl.indexOf('v/') + 2, 11)  

> 好了，基本的东西就这些了，其实也可以自己在firebug中输出一下，然后对比一下就知道怎么回事了，或者输出rss自己对比xml。还是很简单的。