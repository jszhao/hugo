---
title: 'New post: 第三方视频编码服务Zencoder简介及dotnet sdk的使用'
date: 2012-04-14T01:03:00.001-07:00
draft: false
aliases: [ "/2012/04/new-post-zencoderdotnet-sdk.html" ]
---

https://byszsz.com/archives/441.htm?utm\_source=rss&utm\_medium=rss&utm\_campaign=%25e7%25ac%25ac%25e4%25b8%2589%25e6%2596%25b9%25e8%25a7%2586%25e9%25a2%2591%25e7%25bc%2596%25e7%25a0%2581%25e6%259c%258d%25e5%258a%25a1zencoder%25e7%25ae%2580%25e4%25bb%258b%25e5%258f%258adotnet-sdk%25e7%259a%2584%25e4%25bd%25bf%25e7%2594%25a8  
  

> Zencoder是一个优秀的第三方视频编码云服务平台，提供专门的视频编码服务，比如一般的网站要将flv格式视频转换成mp4视频这样的工作，非常占用服务器的CPU，普通网站自己处理很不值得。Zencoder就是专门提供这类服务的一个云平台，使用非常的方便。

Zencoder的服务
-----------

Zencoder提供视频编码的服务，当然，你并不需要把你的视频文件拷贝给他，而是通过API告诉他视频的地址并且授予访问权限（支持http/https/ftp/ftps/sftp/s3），告诉他编码方式，然后告诉他编码完了以后放置在什么位置（支持S3, FTP, FTPS, or SFTP），就可以了。他会自动把视频处理好放置在已设定的位置去。如果你的视频比较大或者像我一样批量提交，希望每个视频处理的结果通知我，他一样支持，你只需要在提交任务的时候，告诉他一个可访问的路径，比如[http://yourdomain/notify.php](http://yourdomain/notify.php)，当然程序你要写写的，他就会在完成每个任务时给予提醒。

Zencoder的服务本身在amazon上，所以针对以S3为存储方式的应用支持良好。本人昨天向zencoder发送了一千五百左右的小视频处理请求（包含许多5秒内的测试视频），在所有任务提交2分钟之后，竟然全部完成了，全部处理耗时不到半小时，神速啊。至于价格，如果你不确定每月处理多少视频，那就是按需处理，每分钟视频5美分的价格；如果每月都处理很大量的视频，可以每月交纳一定的费用，然后按照最低每分钟2美分的价格来使用。

### Zencoder API

Zencoder并没有提供通常的视频处理任务的图形界面，因为比较是一个云服务，并不是针对个人的一两个视频的处理工作。而且创建视频处理的Job时（每一个视频的处理任务称为Job），要提交路径等信息，及时提供完善的图形界面，也无非是填入各种信息然后生成一个api参数Post回去，所以创建Job的过程干脆就做成了生成API参数的样子，与Facebook API的调用测试一样方便。

### Zencoder的API Document

Zencoder的API非常的简单方便，文档写的也很详细，文档路径为[https://app.zencoder.com/docs](https://app.zencoder.com/docs)，里面甚至有很多关于视频格式的资料，不过关于视频格式的问题，还是找视频方面的专家更好些。

Zencoder dotnet SDK
-------------------

.net的SDK非常简单，官方的sdk是于2年前完成的，在github中的zencoder-dotnet [https://github.com/zencoder/zencoder-dotnet](https://github.com/zencoder/zencoder-dotnet)，虽然是2年前完成的，但所有功能都已经完成。不过，怎么说呢，其本身的实现也特别的简单，说白了就是把你生成的API的参数给POST了一下，没做别的更深入的功能（做了也比较多余）。

最简单的用法是：

首先添加引用：

```
 using ZencoderDotNet; using ZencoderDotNet.Api; 
```

然后拼出参数（我们使用JSON格式）：

```
 string json = @"{ ""api\_key"": ""93h630j1dsyshjef620qlkavnmxxxx"", ""input"": ""s3://bucket-name/file-name.avi"" }"; Job job = Job.create(json, "json"); 
```

当然这里的api\_key需要你注册一个帐号使用。如果只是学习下，zencoder提供免费的5秒内视频的编码测试，只是编码后会添加zencoder的测试水印（要在参数中添加”test”:true）。

如果无法访问github，可以从[这里](http://www.everbox.com/f/6oTMhlpbxKE4mBz1wpkNPqrGes)下载

**原创文章，转载请注明：** 转载自[Happiness space](https://byszsz.com/)

**本文链接地址:** [第三方视频编码服务Zencoder简介及dotnet sdk的使用](https://byszsz.com/archives/441.htm)