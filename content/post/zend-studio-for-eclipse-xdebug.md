---
title: 'Zend studio for eclipse + Xdebug开发环境(转载)'
date: 2011-12-28T04:35:00.000-08:00
draft: false
aliases: [ "/2011/12/zend-studio-for-eclipse-xdebug.html" ]
tags : [资源, php]
---

> **转自zendchina，可查看[原文](http://www.zendchina.net/?action-viewnews-itemid-2389)，之所以转载，是因为网上的各种资料都不太全，本文很全面。本人在该问题上也遇到很多困难，终于从本文获得启发。希望遇到同样问题的朋友们能尽快解决该问题。同时声明：网传的windows下不能使用zend调试是错的，很难用也是错的，只是不得法而已，不要以讹传讹。我自己尝试的辛苦，希望朋友们能少走些弯路。**

**下载xdebug，看清版本，很重要，这里使用php\_xdebug-2.0.5-5.2.dll，符件中有;**

必须以Zend方式加载,见php.ini中配置。配置D:EasyPHPPHPphp.ini,先把optimization注释掉使用";"

如下:

;\[Zend\]  
;zend\_optimizer.optimization\_level=1023  
;zend\_extension\_ts="../Zend/ZendExtensionManager.dll"  
;zend\_extension\_manager.optimizer\_ts="../Zend/Optimizer-3.3.0"  
;zend\_extension\_ts="D:/EasyPHP/PHP/ext/ZendDebugger.dll"  
;zend\_debugger.allow\_hosts=127.0.0.1/32  
;zend\_debugger.expose\_remotely=always  
;extension=php\_xdebug-2.0.5-5.2.dll  
\[Xdebug\]  
zend\_extension\_ts=D:/EasyPHP/PHP/ext/php\_xdebug-2.0.5-5.2.dll  
xdebug.profiler\_enable=on  
xdebug.trace\_output\_dir="D:/EasyPHP/xdebug"  
xdebug.profiler\_output\_dir="D:/EasyPHP/xdebug"  
xdebug.remote\_enable=On  
xdebug.remote\_host="localhost"  
xdebug.remote\_port=19000  
xdebug.remote\_handler="dbgp"

请修改端口为19000防止端口被占用，修改web browse,如下图

[![1_201105271110431yKQ8[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/1_201105271110431yKQ81.jpg "1_201105271110431yKQ8[1]")](http://www.zendchina.net/batch.download.php?aid=1140)  
[![02f3aa69-a72c-3f6f-9961-df2175130fd1[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/02f3aa69-a72c-3f6f-9961-df2175130fd11_thumb.jpg "02f3aa69-a72c-3f6f-9961-df2175130fd1[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/02f3aa69-a72c-3f6f-9961-df2175130fd11.jpg)

**不要使用FF会报下面的错误：**

waiting for XDebug seession...就不动了，选择IE正常  
[![3b626781-cc30-3c93-a00a-37971669368f[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/3b626781-cc30-3c93-a00a-37971669368f1_thumb.jpg "3b626781-cc30-3c93-a00a-37971669368f[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/3b626781-cc30-3c93-a00a-37971669368f1.jpg)

[![1_201105271110432Amea[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/1_201105271110432Amea1.jpg "1_201105271110432Amea[1]")](http://www.zendchina.net/batch.download.php?aid=1141)

然后再按下面配置：

**另外:  
**

**Zend Studio for Eclipse开启XDebug的方法:**

**\- 6.0.0pluginscom.zend.php\_6.0.0.v20080107plugin\_customization.ini**

**将这行org.eclipse.ui.workbench/UIActivities.com.zend.php.debug.ui.XDebugActivity=false  
改成true，保存后，重新启动Zend Studio 7，php debug里面就可以选择Xdebug进行调试了。**

[![1_201105271110433rX2W[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/1_201105271110433rX2W1.jpg "1_201105271110433rX2W[1]")](http://www.zendchina.net/batch.download.php?aid=1142)  
[![cfcf2c2e-e88f-3587-aa2a-8fd36d92d2cf[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/cfcf2c2e-e88f-3587-aa2a-8fd36d92d2cf1_thumb.jpg "cfcf2c2e-e88f-3587-aa2a-8fd36d92d2cf[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/cfcf2c2e-e88f-3587-aa2a-8fd36d92d2cf1.jpg)

[![1_201105271110434SBhM[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/1_201105271110434SBhM1.jpg "1_201105271110434SBhM[1]")](http://www.zendchina.net/batch.download.php?aid=1143)  
[![404fa9ad-6b50-300e-8397-8aed9bfb24df[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/404fa9ad-6b50-300e-8397-8aed9bfb24df1_thumb.jpg "404fa9ad-6b50-300e-8397-8aed9bfb24df[1]")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/404fa9ad-6b50-300e-8397-8aed9bfb24df1.jpg)

[![1_201105271110435A1DJ[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/1_201105271110435A1DJ1.jpg "1_201105271110435A1DJ[1]")](http://www.zendchina.net/batch.download.php?aid=1144)

选择要debugger的web页面

[![1_201105271110436fT64[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/1_201105271110436fT641.jpg "1_201105271110436fT64[1]")](http://www.zendchina.net/batch.download.php?aid=1145)[![1_201105271110436fT64[1][1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/1_201105271110436fT6411.jpg "1_201105271110436fT64[1][1]")](http://www.zendchina.net/batch.download.php?aid=1145)[![1_201105271110436fT64[1][2]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/1_201105271110436fT6412.jpg "1_201105271110436fT64[1][2]")](http://www.zendchina.net/batch.download.php?aid=1145)[![1_201105271110437mM47[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/1_201105271110437mM471.jpg "1_201105271110437mM47[1]")](http://www.zendchina.net/batch.download.php?aid=1146)[![1_201105271110437mM47[1][1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/1_201105271110437mM4711.jpg "1_201105271110437mM47[1][1]")](http://www.zendchina.net/batch.download.php?aid=1146)[![1_201105271110437mM47[1][2]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/1_201105271110437mM4712.jpg "1_201105271110437mM47[1][2]")](http://www.zendchina.net/batch.download.php?aid=1146)[![1_2011052711104380406[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/1_20110527111043804061.jpg "1_2011052711104380406[1]")](http://www.zendchina.net/batch.download.php?aid=1147)[![1_2011052711104380406[1][1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/1_201105271110438040611.jpg "1_2011052711104380406[1][1]")](http://www.zendchina.net/batch.download.php?aid=1147)[![1_2011052711104380406[1][2]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/1_201105271110438040612.jpg "1_2011052711104380406[1][2]")](http://www.zendchina.net/batch.download.php?aid=1147)[![1_2011052711104397eht[1]](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/1_2011052711104397eht1.jpg "1_2011052711104397eht[1]")](http://www.zendchina.net/batch.download.php?aid=1148)

另外WinCacheGrind的使用参考：http://hi.baidu.com/lostdays/blog/item/c2ef51a920c62ff81f17a2f4.html

我的phpinfo信息如下，方便出现问题对比版本：

### PHP Version 5.2.5

System  
Windows NT WV 5.1 build 2600

Build Date  
Nov 8 2007 23:18:08

Configure Command  
cscript /nologo configure.js "--enable-snapshot-build" "--with-gd=shared"

Server API  
Apache 2.0 Handler

Virtual Directory Support  
enabled

Configuration File (php.ini) Path  
C:WINDOWS

Loaded Configuration File  
D:EasyPHPPHPphp.ini

PHP API  
20041225

PHP Extension  
20060613

Zend Extension  
220060519

Debug Build  
no

Thread Safety  
enabled

Zend Memory Manager  
enabled

IPv6 Support  
enabled

Registered PHP Streams  
php, file, data, http, ftp, compress.zlib, compress.bzip2, zip

Registered Stream Socket Transports  
tcp, udp

Registered Stream Filters  
convert.iconv.\*, string.rot13, string.toupper, string.tolower, string.strip\_tags, convert.\*, consumed, zlib.\*, bzip2.\*