---
title: 'backbonejs中this的绑定问题—-backbonejs学习笔记'
date: 2013-08-28T02:51:00.001-07:00
draft: false
aliases: [ "/2013/08/backbonejsthis-backbonejs.html" ]
---

最近开始学习backbonejs。 学习中遇到的种种问题，以笔记形式记录在此。今天是第一篇笔记，记录比较零碎，勿怪。 view中的经常会发现this及其属性在不同的方法中不一致的问题，这是因为针对不同的方法，this指向的并不是本view。 解决方法也很简单： \_.bindAll(this, 'render'); 这里假设要绑定的是render方法，如果要绑定多个的话，直接用逗号隔开即可\_.bindAll(this, 'render','anotherfunction');。

  
  
[backbonejs中this的绑定问题—-backbonejs学习笔记](https://byszsz.com/archives/597.htm?utm_source=rss&utm_medium=rss&utm_campaign=backbonejs%25e4%25b8%25adthis%25e7%259a%2584%25e7%25bb%2591%25e5%25ae%259a%25e9%2597%25ae%25e9%25a2%2598-backbonejs%25e5%25ad%25a6%25e4%25b9%25a0%25e7%25ac%2594%25e8%25ae%25b0)