---
title: 'Apache:基于名称的虚拟主机和基于IP的虚拟主机'
date: 2011-10-19T11:30:00.000-07:00
draft: false
aliases: [ "/2011/10/apacheip.html" ]
tags : [apache, php]
---

> Apache支持两种形式的虚拟主机，分别是基于名称的虚拟主机和基于IP的虚拟主机，今天下午经过几次实验，终于调试通过。现分析给大家：

1、基于名称的虚拟主机（NameVirtualHost）
----------------------------

基于名称的虚拟主机，是根据不同的ServerName，将站点指向不同的目录的一种形式。一般适用于只拥有一个IP，但拥有多个域名的情况。配置基于名称的虚拟主机，需要在httpd.conf中添加如下配置（此处以802端口为例）：

```
NameVirtualHost \*:802  
  
<VirtualHost \*.802>  
    ServerAdmin webmaster@zsz.com  
    DocumentRoot "D:/program files/htdocs/fbsdk/"  
    ServerName www.fb.com  
    ServerAlias www.fb.com  
</VirtualHost>
```  
  

在这里需要说一下的是：<VirtualHost \*.802> 这一段千万不要写成<VirtualHost [www.fb.com.802](http://www.fb.com.802)\>，本人深受其害，搞了半天才发现问题的症结，耽误了好些时间。

  
  

  
  

2、基于IP地址的虚拟主机
-------------

  
  

基于IP的虚拟主机，是根据不同的IP，将站点指向不同目录的一种形式。适用于有多个ip和域名，且每个域名拥有一个独立IP的情况。具体写法如下（此处以端口801为例）：

  
  
```
Listen www.fb.com:801  
  
  
<VirtualHost www.fb.com:801>  
DocumentRoot "D:program filesxampphtdocsfbsdk"  
ServerName www.fb.com:801  
ServerAdmin www@fb.com  
</Virtualhost>
```  
  

这里，第一行可以直接写做

  
  

>   
> 
> Listen 801
> 
>   

  
  

而ServerName的位置也可以不写端口号。

  
  

比较下代码，其实不难发现，前面我所出现的问题，其实是因为缩写的代码造成冲突引起的。基于名称的虚拟主机的配置中，代码段<VirtualHost \*.802> 如果写成<VirtualHost [www.fb.com.802](http://www.fb.com.802)\>，那么就与基于IP地址的虚拟主机前面的写法一致，而相关的配置又不配套，势必造成混乱。

  
  

参考资料：

  
  

1、[http://httpd.apache.org/docs/2.2/vhosts/](http://httpd.apache.org/docs/2.2/vhosts/ "http://httpd.apache.org/docs/2.2/vhosts/")