---
title: '解决WordPress的“正在执行例行维护，请一分钟后回来”'
date: 2012-02-28T15:05:00.000-08:00
draft: false
aliases: [ "/2012/02/wordpress.html" ]
tags : [web]
---

> 昨天登录wordpress后台，发现提示有个插件有更新，本人向来喜欢最新的东西，有更新自然就忍不住要更新，就随手点了更新，结果不知咋回事更新了半天也没更新完，无奈刷新页面，网站却提示“正在执行例行维护，请一分钟后回来”。

  
有问题要解决啊，搜索下吧，发现月光也遇到过类似的问题，不过他那个是更新主题的时候出的问题，解决办法就是删除站点根目录下的“.maintenance”的文件，我就照做了，但是问题并没有解决，而是直接报错了，报错的路径是所更新的插件的路径。  
  
之前我也更新过其他插件，而且站点的所有插件都是在线安装的，并没有出过什么问题，所以我认为程序并没有出错，很有可能是更新过程中，某些文件写了一半，却因为某些原因（比如虚拟主机临时故障）中断了，造成插件加载失败。于是将该插件的目录删除，并手工上传了一份最新的插件进去，结果就好了。  
  
同时，这里贴一下网上其他朋友遇到问题时的解决办法：  
  
首先，打开你的web文件，找到 /wp\_admin/includes/class-wp-filesystem-direct.php 文件，修改它，没有权限请修改文件夹属性为777，然后去修改**function mkdir**这个函数，用下面的代码替换原有的代码：  
```
function mkdir($path,$chmod=false,$chown=false,$chgrp=false){  
if( ! $chmod)  
$chmod = $this->permission;  
// Fix “Cound not create directory” problem  
if(ini\_get(‘safe\_mode’) && substr($path, -1) == ‘/’)  
{  
$path = substr($path, 0, -1);  
}  
// Fix “Cound not create directory” problem  
if ( ! @mkdir($path) )  
return false;  
$this->chmod($path, $chmod);  
if ( $chown )  
$this->chown($path, $chown);  
if ( $chgrp )  
$this->chgrp($path, $chgrp);  
return true;  
}
```