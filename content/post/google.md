---
title: 'Google+邀请'
date: 2011-08-27T06:59:00.000-07:00
draft: false
aliases: [ "/2011/08/google.html" ]
tags : [资源]
---

上个月被邀请了加入google+，也没怎么玩，不久前有朋友咨询有没有邀请，发现还是有部分朋友无法注册，本人有一些邀请，如果谁还需要邀请，就留言留下你的邮箱就可以了。  
  
附访问方法：  
  
（C:windowssystem32driversetc）添加以下内容既可  
  
#------ Google+ ------  
203.208.46.29 plus.google.com  
203.208.46.29 talkgadget.google.com  
203.208.46.29 picasaweb.google.com  
203.208.46.29 lh1.ggpht.com  
203.208.46.29 lh2.ggpht.com  
203.208.46.29 lh3.ggpht.com  
203.208.46.29 lh4.ggpht.com  
203.208.46.29 lh5.ggpht.com  
203.208.46.29 lh6.ggpht.com  
203.208.46.29 lh6.googleusercontent.com  
203.208.46.29 lh5.googleusercontent.com  
203.208.46.29 lh4.googleusercontent.com  
203.208.46.29 lh3.googleusercontent.com  
203.208.46.29 lh2.googleusercontent.com  
203.208.46.29 lh1.googleusercontent.com