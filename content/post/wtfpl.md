---
title: '一个奇怪的许可证WTFPL'
date: 2013-08-27T19:07:00.003-07:00
draft: false
aliases: [ "/2013/08/wtfpl.html" ]
---

发现一个特殊的许可证WTFPL
===============

[http://www.wtfpl.net/](http://www.wtfpl.net/)

不想翻译了，直接贴出来：
============

About the WTFPL
===============

Posted December 26th, 2012 by [Sam Hocevar](http://www.wtfpl.net/author/admin/ "Posts by Sam Hocevar"). The _Do What The Fuck You Want To Public License_ (WTFPL) is a free software license. There is a long [ongoing battle](http://www.google.com/search?q=bsd+vs+gpl) between [GPL](http://www.gnu.org/copyleft/gpl.html) zealots and [BSD](http://www.freebsd.org/copyright/license.html) fanatics, about which license type is the most free of the two. In fact, both license types have unacceptable obnoxious clauses (such as reproducing a huge disclaimer that is written in all caps) that severely restrain our freedoms. The WTFPL can solve this problem. When analysing whether a license is free or not, you usually check that it allows free usage, modification and redistribution. Then you check that the additional restrictions do not impair fundamental freedoms. The WTFPL renders this task trivial: it allows everything and has no additional restrictions. How could life be easier? You just DO WHAT THE FUCK YOU WANT TO.

### Full license text

> ```
>   `          DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE                      Version 2, December 2004      Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>      Everyone is permitted to copy and distribute verbatim or modified   copies of this license document, and changing it is allowed as long   as the name is changed.                 DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE     TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION       0. You just DO WHAT THE FUCK YOU WANT TO.  `  
> ```

  
  
[一个奇怪的许可证WTFPL](https://byszsz.com/archives/569.htm?utm_source=rss&utm_medium=rss&utm_campaign=%25e4%25b8%2580%25e4%25b8%25aa%25e5%25a5%2587%25e6%2580%25aa%25e7%259a%2584%25e8%25ae%25b8%25e5%258f%25af%25e8%25af%2581wtfpl)