---
title: '垃圾的华夏银行U盾'
date: 2012-04-08T08:09:00.000-07:00
draft: false
aliases: [ "/2012/04/u.html" ]
tags : [闲言碎语]
---

> 华夏银行的U盾在广告中宣称：与其他U盾不同，使用其U盾不用到网站上下载驱动，言外之意，是使用方便呗。今天真正使用时，发现其实不然。

  
说明书中称：安装U盾需要三个步骤，当然都是很简单很必须的。打开他的U盾，发现其中设置了个自动启动的程序，就是要安装驱动。哦，原来这就是与其他U盾不同的地方啊。搞一个U盘，把驱动放进去，就算先进了？还要拿到广告里面宣传一番！  
  
这下好，驱动省得下载了，（其实其他银行的也没见一定要下载的，大多都是自动安装的）。不同的是，别人的U盾自动完成后就可以用了，但华夏的不可以！  
  
插上U盾后，还有8个步骤要走，要下载一大堆的驱动和证书，而且全部都是手动下载啊，这就是你们所谓的方便么？！  
  
最可气的是，这8个步骤所要下载的内容在网上没有。  
  
比如，安装cfca数字证书链，这个完全应该不用手动控制的，你的U盘干啥去了？好嘛，我去下载，不过网页上已经改名字了，咱会猜，能猜到。  
  
下一个呢，个人网银安全控件，网页上根本没有，倒是有俩名字完全一样的下载程序，无法区分哪个是，下载后发现大小不一致，俩都安装了吧！要重启啊！  
  
不甘心真的重启了，发现使用证书打开时，反而打不开网页了。  
  
这帮人就是脑残，既然使用证书，干嘛还非得跟U盾结合起来。用了U盾，要什么证书登录。看其说明的最后一步，要用U盾在证书用户中登录，还得使用用户名、密码、验证码，我的个天哪，啥都得输入，你搞个U盾还有证书糊弄用户呢吧？  
  
现在真的有些公司，明明自己没啥技术，却非搞得客户很麻烦，麻烦以后，客户就安全了？一点都不知道踏踏实实做事，难道就没见过人家别的银行网站咋登录的么？