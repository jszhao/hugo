---
title: '部分开放平台url分享统计api'
date: 2011-09-23T11:31:00.000-07:00
draft: false
aliases: [ "/2011/09/urlapi.html" ]
tags : [开放平台, api]
---

发现一些与分享url有关的api接口，记录下

> Facebook - [http://graph.facebook.com/?ids=http://www.seomoz.org](http://graph.facebook.com/?ids=http://www.seomoz.org)  
> Twitter - [http://urls.api.twitter.com/1/urls/count.json?url=http://www.seomoz.org](http://urls.api.twitter.com/1/urls/count.json?url=http://www.seomoz.org)  
> Linkedin - [http://www.linkedin.com/cws/share-count?url=http://www.seomoz.org](http://www.linkedin.com/cws/share-count?url=http://www.seomoz.org)  
> Stumbleupon - [http://www.stumbleupon.com/services/1.01/badge.getinfo?url=http://www.seomoz.org](http://www.stumbleupon.com/services/1.01/badge.getinfo?url=http://www.seomoz.org)  
> Delicious - [http://feeds.delicious.com/v2/json/urlinfo/data?url=http://www.seomoz.org](http://feeds.delicious.com/v2/json/urlinfo/data?url=http://www.seomoz.org)  
> Google Buzz - [https://www.googleapis.com/buzz/v1/activities/count?alt=json&url=http://www.seomoz.org](https://www.googleapis.com/buzz/v1/activities/count?alt=json&url=http://www.seomoz.org)  
> Reddit - [http://www.reddit.com/api/info.json?url=http://www.seomoz.org](http://www.reddit.com/api/info.json?url=http://www.seomoz.org)  

参考资料：

1、Seo blog：[http://www.seomoz.org/blog/how-to-track-your-social-media-strategy](http://www.seomoz.org/blog/how-to-track-your-social-media-strategy "http://www.seomoz.org/blog/how-to-track-your-social-media-strategy")