---
title: 'source map 简介(翻译)'
date: 2014-04-18T01:26:00.001-07:00
draft: false
aliases: [ "/2014/04/source-map.html" ]
---

原文地址：

[introduction-source-maps](http://blog.teamtreehouse.com/introduction-source-maps?utm_source=CSS-Weekly&utm_campaign=Issue-91&utm_medium=email)

翻译的不好，见谅。另注：关于文中提到的，这里有一篇中文的[介绍](http://www.zhangxinxu.com/wordpress/2013/01/uglifyjs-compress-js/)

译文：

我们常常使用一个提升站点性能的办法，就是把js文件和css文件打包压缩。但是压缩以后如果我们要调试或者修改呢？那简直是恶梦。不过，现在有一种可以部分解决这个问题的方式，叫做Source Map，我们翻译成源码图吧。

Source Map可以把压缩的文件转换成压缩前的样子。这就意味着，你可以尽管压缩你的代码，然后还可以方便的调试源码。现在chrome和firefox浏览器的开发工具已经支持SourceMap了。

本文介绍SourceMap的工作原理和生成方法，我们只介绍js的sourcemap。

备注：firefox默认支持SourceMap，Chrome浏览器需要手动开启支持。设置方法：在chrome浏览器的控制面板里（settings），里面的常规选项卡里有Enable js sourcemap 和Enable css source map 两个选项，勾选即可。

Source Map 工作方式

顾名思义,源图由一大堆信息组成,把压缩的代码转换成阅读的原始代码。您可以指定一个不同的源映射。做法就是把压缩的代码中加入一句注释：

```
//# sourceMappingURL=/path/to/script.js.map
```

支持Source Map的js压缩工具会自动加入这句注释，浏览器只在开发工具打开的时候才会识这句注释并加载map文件（如果是chrome浏览器，则需要设置打开）。

map文件其实是一个json格式的文件，包含一些相关性的信息，示例如下：

```
{  
    version: 3,  
    file: "script.js.map",  
    sources: [  
        "app.js",  
        "content.js",  
        "widget.js"  
    ],  
    sourceRoot: "/",  
    names: ["slideUp", "slideDown", "save"],  
    mappings: "AAA0B,kBAAhBA,QAAOC,SACjBD,OAAOC,OAAO..."  
}
```

每个节点的含义如下：

*   version，版本号
*   file，文件名
*   sources,源码文件，支持多个
*   sourceRoot，源码目录
*   names，包含所有变量名、方法名等信息的列表，这是压缩时被替换掉的
*   mappings,一组base64编码的字符串

用UgilfyJS压缩工具生成map文件

UgilfyJS是一个优秀的压缩js的命令行工具，版本2以上的版本支持SourceMap，使用方式为：

```
uglifyjs [input files] -o script.min.js --source-map script.js.map --source-map-root http://example.com/js -c -m
```

*   `--source-map` – 输出的文件名.
*   `--source-map-root` – (optional) `sourceRoot`属性.
*   `--source-map-url` – (optional) url. `//# sourceMappingURL=/path/to/script.js.map`
*   `--in-source-map` – (optional) 源路径
*   `--prefix` or `-p` – (optional)从 `sources` 移除`n` 级路径 . 比如, `-p 3` 会移除三级路径, 比如 `one/two/three/file.js` 会变成 `file.js`. 使用`-p relative`会使用相对路径.

grunt 有个ugilfyJS插件，可参考[对应文档](https://github.com/gruntjs/grunt-contrib-uglify#sourcemap)使用。

Chrome Dev Tools查看的效果
---------------------

[![The Sources Tab in Chrome Dev Tools](http://blog.teamtreehouse.com/wp-content/uploads/2013/12/chrome-tools.png)](http://blog.teamtreehouse.com/wp-content/uploads/2013/12/chrome-tools.png)

The Sources Tab in Chrome Dev Tools

（本人电脑可能是版本问题，并未出现对应的js）

[TRY A DEMO](http://demos.mattwest.io/source-maps/)

Firefox Developer Tools中查看效果
----------------------------

[![The Debugger Tab in the Firefox Developer Tools](http://blog.teamtreehouse.com/wp-content/uploads/2013/12/firefox-tools.png)](http://blog.teamtreehouse.com/wp-content/uploads/2013/12/firefox-tools.png)

The Debugger Tab in the Firefox Developer Tools

Firefox的开发工具的debugger可以看到，不过当你尝试的时候会发现只有当打开对应的js时才会出现内容，之前不会加载出来（或者说渲染出来）.

[TRY A DEMO](http://demos.mattwest.io/source-maps/)

后记
--

Source Map允许程序员优化代码的同时又能够方便的调试源代码，着实是个好东西.

这篇文章压缩js用到了UglifyJS.如果你还没有用过UglifyJS优化你的网站，可以把这个工具加入你的工作流中。