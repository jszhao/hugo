---
title: '关于个人密码安全'
date: 2012-04-03T10:04:00.000-07:00
draft: false
aliases: [ "/2012/04/blog-post_910.html" ]
tags : [闲言碎语]
---

> 其实最近一直在考虑关于个人密码安全的问题，而且我本来也是对各种密码还算比较注意的，但今天突然发现密码被盗了。所以我今天所说的安全措施绝对不是成功的保护措施，所讲的内容更不是什么传教。我把我的方法分享下，如果你觉得有可取之处，我会觉得很好；如果你觉得不足并能指出给我，那太感谢了。

  
首先，如果你使用多个网站，脑子不一定记得住所有的密码。  
  
一个人一生要记住多少密码？使用一个网上银行，要记住取款密码、电话银行密码、网银登录密码、网银交易密码。第一次设置的时候，银行提醒密码设置要不相同，同时记录4个密码，还不算困难，但是重点是这东西你有不是天天用，哪里能记得住？有一次要修改交易密码（交行），对方要我输入电话银行密码，这个真要难度，我就注册的时候用过一次，5年了啊，你以为我是脑子是磁带机啊。后来我到柜台去修改密码，交行的小同志直接说，那么多密码，设置成一个就行！  
  
开始只有一个银行卡，密码从来都是记在脑子里，后来多办了三四张，又搞了五六个信用卡，这就应付不过来了。其实密码安全和方便的思考，从那时候就开始了。  
  
怎么呢？搞一个记事本，把所有的东西都记录在里面，当然，密码不要直接写进去，给自己个提示就可以了，就是说脑子里面还得记录好几套算法，看到哪个这个提示，你就得能想到这密码是什么。当然这套算法不能太困难，所以呢，记事本还要加密一下，怎么加密呢？很简单，用winrar压缩一下，压缩时再设置一个复杂的密码就可以了，这个密码可要记好了啊，这才记一个密码啊！  
  
好了，加密好了，这个记事本放在那里啊？建议放在U盘里，做两份，分别锁进两个抽屉，用的时候再拿出来！所以家里常备几个小容量的U盘是很必要的。现在银行使用的所谓U盾，其实就是这么个思路。  
  
这样很麻烦啊。谁让网络环境这样呢。当然，我还想过另外一个办法，就是电脑做双系统，搞一个xp，只在使用网银的时候开，磁盘使用NTFS格式化，设置其他人不可访问个人信息，平常使用win7，网银用xp（当年许多网银只支持IE6，不过现在好了）。不过这个我也嫌麻烦，没实施过。  
  
网银嘛，可以麻烦点，日常使用的网站可不能那么麻烦，那又得怎么办呢？我们得把普通网站分个类：  

  
*   可有可无的，没有个人数据。
  
*   常用的邮箱、qq、msn等包含大量联系人或好友信息。
  
*   微博、博客等有大量个人信息的网站。
  

  
可有可无的，那就搞一个统一的密码和用户名，比如用户名密码都叫做张三，有人爱盗，就盗去吧；  
  
常用的邮箱就不一样，之前注册都要验证邮箱，不太重要的网站密码忘了就忘了，可以通过邮箱找回，你的邮箱密码被盗了，那就完了，所以常用邮箱要绑定手机、绑定密保卡、绑定所有能绑定的东西！哈哈，说大了，因为邮箱是常用的东西，密码泄露是很有可能的，但要是安全设置到位，还是能找回来的。当然，邮箱密码的复杂度一定要够，要足够长，要定期换，因为这个也是唯一一个常用的密码了。  
  
其他所有的一般的网站，微博啥的，盗了也无所谓，只要能通过邮箱找回，密码可以设置复杂一些，也不用全记住，不过毕竟也没啥好盗取的。（所以我特别纳闷我的微博帐号为啥会被盗，因为我很少登录，一般也只会在我的touch上开一下）。  
  
对于这些不是太重要，又还要常常登录的网站，每次找回密码来登陆，确实也很麻烦，如果你有大量的用户名和密码，可以使用以下的密码记录和管理工具（这里声明下：所有的密码记录管理工具都不是足够安全的，）。  

  
*   chrome浏览器可以记住你的密码，（废话，哪个浏览器都能）。chrome浏览器有一个登录同步的功能，表面看可以同步收藏夹的功能，实际上也可以同步所记录的密码。他的缺点是不能查看和管理密码。也就是说缺少个密码管理器，只记录了密码，而且保存的这个密码似乎是明文。别的浏览器也有这个功能，而且应该大家都在各自的浏览器上用着呢，chrome的同步功能很不错，但你也可以说google偷走了你的密码（他应该犯不上）。
  
*   1Password，以浏览器插件的形式，每次你修改密码或登录的时候，捕获到你的信息输入，可以选择性的保存。你需要使用密码的时候，可以打开来看，这个默认是关闭的，输入密码才会打开（这个密码要记住）。另外，他会自动匹配你的dropbox，dropbox会自动把加密的密码文件同步到你的dropbox中，所以实现了在多台计算机中共享密码的。这也说明了1Password不像google那样把你的密码同步到他的服务器上去。什么，dropbox是什么？现在没用过dropbox的人可真不好找，赶快用我的[邀请链接](http://db.tt/iYTGHY8U)注册一个吧，也算帮我的忙，阿弥陀佛。这里你还得记住dropbox的密码才行，还好你的客户端可以记住。缺点是每次用都得输入密码，又麻烦又安全
  
*   LastPass，他跟1Password有些类似，但有个更强大（或者说恐怖）的功能，读取你浏览器中记录的用户名密码，存入密码表中，你打开网页时他会根据域名自动选择要自动填写、自动登录、或者全手动不记录。他号称有了LastPass，从此只需要一个密码（LastPass）就可以安全又方便的生活在这个互联网中了。使用的时候，只要一次打开LastPass，输入密码，只要浏览器不关，就可以一直使用。而且他会自动把你的密码表加密后同步到他的云端，这只是他说，是不是加密了，我也不知道哦，不过因为一丁点的安全问题，谷歌都成被告了，这些国外的公司不敢随便乱来的，又不是天朝的公司，没人管。缺点是:：太方便了，如果有人使用你的计算机，也很容易看到你的密码。
  

  
注意：后两种插件建议只安装最常用的浏览器的插件，不要安全ie的插件，ie还要用来上网银呢。事实上，我的做法是，IE只用于上网银，别的时候不开；火狐只用于工作，调试个脚本啥的，也是火狐的强项；chrome用来平时上网最方便不过，插件只安装在这上面安全又方便。  
  
还有，这个插件实际上捕获不到安全做到极致的网站的密码（比如使用https登录的网站），所以也不同太担心它会保留你的常用邮箱的密码或网银的密码。  

> 再次强调一下，方便的东西一定要只用于不重要的内容。