---
title: 'JsonObject.Parse 接收json数组时的问题'
date: 2013-06-28T01:36:00.009-07:00
draft: false
aliases: [ "/2013/06/jsonobjectparse-json.html" ]
---

访问twitter的信息，返回的结果是个数组：

```
  \[  
```

```
  {  
```

```
  ...  
```

```
  },{  
```

```
  ...  
```

```
  }  
```

```
  \]  
```

```
      
结果在windowsRT的JsonObject.Parse时发现程序报错。经测试发现该方法Parse根是数组的json对象确实会出现问题，而且Json.Net的组件同样会发生问题。    
既然直接Parse数组不成，把它拼成个对象呗：  
``````
  content = "{\\"result\\":"+ content+"}";  
```

```
  JsonObject jsonObject = JsonObject.Parse(content);  
``````
      
OK了  
```

**原创文章，转载请注明：** 转载自[Happiness space](https://byszsz.com/)

**本文链接地址:** [JsonObject.Parse 接收json数组时的问题](https://byszsz.com/archives/521.htm)

```
      
  
[JsonObject.Parse 接收json数组时的问题](https://byszsz.com/archives/521.htm?utm_source=rss&utm_medium=rss&utm_campaign=jsonobject-parse-%25e6%258e%25a5%25e6%2594%25b6json%25e6%2595%25b0%25e7%25bb%2584%25e6%2597%25b6%25e7%259a%2584%25e9%2597%25ae%25e9%25a2%2598)  
```