---
title: 'Facebook专页中的页面制作'
date: 2011-12-10T06:16:00.000-08:00
draft: false
aliases: [ "/2011/12/facebook.html" ]
tags : [开放平台, facebook]
---

> 关于facebook的项目停了一段时间，最近突然又来了一个这类的项目，是要制作一个专页上的链接。话说创建专页不难啊，但是由于不懂，而且时间比较紧，遇到不少问题，至今还有些搞不明白。幸得本站用户KIVU的鼎力帮助，项目算顺利完成了。这里把制作过程记录下来，分享给大家，以减少大家走的弯路。

首先，专页是facebook专门推出用来展示个人或者企业的一个工具，创建专页在facebook上面有专门的帮助文档，很详细（不过也很多，说实话，我没有详细去看，创建专页自己试试就可以创建出来），url如下：

```
[http://www.facebook.com/help/?faq=104002523024878&ref\_query=crea](http://www.facebook.com/help/?faq=104002523024878&ref_query=crea)
```  
  

  
  

我们可以看到很多公司做了挺漂亮的专页，但我们创建的专页却仍然只是由涂鸦墙、视频等信息组成的页面，我们自己的页面怎么实现呢？或者说，在左侧上部的链接中，加入我们自己制作的页面。

  
  

#### 创建新应用做专业中的页面

  
  

这里要感谢本站用户KIVU，一句话说的很明白，左侧的链接都是应用，也就是facebook app。其中涂鸦墙等几个是facebook自己的app，也有几个是必须显示不可删除的，添加怎么办呢？Facebook似乎没有提供一个地方要指定加入一个应用的地方。感觉把专页中的设置尝试遍了，也没找到对应的功能。

  
  

KIVU在百忙之中给予支持：

  
  

1、创建新应用（前面我们有文章提到过），在编辑应用基本信息的界面左下角，找到“View App Profile Page”这个链接，现在即使是简体中文的界面，这个链接的文字也没有翻译，应该是查看我的应用的专页吧。

  
  

2、进入该页面以后（url如[http://www.facebook.com/apps/application.php?id=XXX](http://www.facebook.com/apps/application.php?id=XXX "http://www.facebook.com/apps/application.php?id=308980685790758")），可以在其左下角找到“加入到我的专业链接”，其实这是一个按钮，点击后会弹出你所有的可以被加入的专页（包括其他应用）。如图：

  
  

[![addtofbpage](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/addtofbpage_thumb.png "addtofbpage")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/addtofbpage.png)

  
  

找到你想加入的页面，点 add to page就可以了。至于页面中的内容，就是你的app中指定的页面的内容了。还算简单吧。

  
  

3、往往你制作好页面以后，页面高度没有按照自己的页面来，就是说即使你设置了高度自适应（那似乎只对应用页面有效，对专业中的页面无效），页面中还是出现了滚动条，其实也很简单，我们只需要写这样一段代码就可以了

  
  
```
FB.Canvas.setSize({ width: 520, height: 2400 });
```  
  

  
  

这里要记住啊，width：520基本上是必须这么写的，因为专页中的页正好是520的宽度。很有爱啊。对了别忘了，要加入facebook js api的引用啊：

  
  
```
<script src="http://connect.facebook.net/en\_US/all.js#xfbml=1"></script>
```  
  

  
  

还有一点要说明，刚创建的应用好像点查看我的应用专页时会直接进入应用详细页，要等几个。我昨天创建的应用就这样，害得我好苦，不过今天都可以点进去了。所以有些东西是不可仓促行事的，必要的时间是需要留出来的。

  
  

#### 使用纯静态的页面

  
  

如果你的页面是纯静态的话，还有一种实现方式，就是使用别人做的应用来帮忙实现。当然了，这是其他人做好了的应用，facebook有好多此类的应用。比如我们在facebook中搜索FBML：

  
  

[![staticfbml](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/staticfbml_thumb.png "staticfbml")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/staticfbml.png)

  
  

可以看到有许多做静态FBML的应用，随便找一个把它加入到你的专页中去，然后在编辑专业的应用程序栏找到该应用，如图：

  
  

[![editpage1](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/editpage1_thumb.png "editpage1")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/editpage1.png)

  
  

然后点击访问应用程序，可以到编辑FBML页面的地方，如图：

  
  

[![editfbml2](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/editfbml2_thumb.png "editfbml2")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/12/editfbml2.png)

  
  

如果你的页面足够简单，使用该方法是可以非常轻易制作一个应用的页面，而且不需要自己准备服务器，够爽吧？

  
  

当然，如果你的页面中脚本比较多，可能并不适合你，因为需要学习许多FBML中js的用法，我还没用过，也还不清楚是否麻烦。

  
  

>   
> 
> 好了，本文通过两种方式实现了专页中的个性页面，实际上实现过程中还遇到了rss、youtube和twitter中的问题，我会在后面的文章中继续与大家分享。