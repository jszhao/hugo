---
title: '专为制作的网易邮箱签名设置页面'
date: 2012-03-17T08:48:00.000-07:00
draft: false
aliases: [ "/2012/03/blog-post_6432.html" ]
tags : [资源]
---

> 上一篇写了网易邮箱的签名设置方法，普遍反馈太难了，为此，今天特制作一个邮箱设置的页面，用jquery实现的。

第一步：设置签名模板
==========

下面是一个邮箱模板：

[模板链接](https://byszsz.com/demo/sign/ssreader.htm "打开模板页面")

点击该链接进去，可以看到一个签名模板，你可以点击其中的姓名，在弹出的输入框里写上你自己的姓名。如图：![](http://image.cdn.albums/albums/ac265/zsz417/blog/prompt.png)

填写你的名字，同理，你还可以在部门和电话的位置分别填写部门和电话，填写成功后检查是否正确，下面是我填写的一个示例：

![](http://image.cdn.albums/ac265/zsz417/blog/demoforemailsign.png)

这是后签名就已经设置好了。

第二步：设置邮箱中的签名。
=============

复制这段文字，要从名字一直复制到最后的公司两个字，如图

![](http://image.cdn.albums/albums/ac265/zsz417/blog/copysign.png)

右键选择复制，然后进入你的邮箱，在前面设置的位置，将复制的内容粘贴保存即可。如图：

![](http://image.cdn.albums/albums/ac265/zsz417/blog/savesign.png)

这次还算简单吧。