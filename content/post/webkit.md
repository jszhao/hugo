---
title: 'Webkit内核自定义滚动条样式'
date: 2012-03-04T02:01:00.000-08:00
draft: false
aliases: [ "/2012/03/webkit.html" ]
tags : [webkit, web]
---

> 最近的项目中要自定义滚动条，之前这类的工作也做过，但是这次项目紧啊，看到这个设计一直在发愁。好在这次项目只需要兼容最新的chrome浏览器就可以，所以直接查找了下Webkit下滚动条的css写法，感觉不错。记录下来。

直接上css属性吧，咱又不是搞css的，也讲不明白，哈哈。

:horizontal – horizontal伪类应用于水平方向的滚动条

:vertical – vertical伪类应用于竖直方向的滚动条

:decrement – decrement伪类应用于按钮和内层轨道（track piece）www.oolong.com.cn。它用来指示按钮或者内层轨道是否会减小视窗的位置（比如，垂直滚动条的上面，水平滚动条的左边。）

:increment – increment伪类和decrement类似，用来指示按钮或内层轨道是否会增大视窗的位置（比如，垂直滚动条的下面和水平滚动条的右边。）

:start – start伪类也应用于按钮和滑块。它用来定义对象是否放到滑块的前面。

:end – 类似于start伪类，标识对象是否放到滑块的后面。

:double-button – 该伪类以用于按钮和内层轨道。用于判定一个按钮是不是放在滚动条同一真个一对按钮中的一个。对于内层轨道来说，它表示内层轨道是否紧靠一对按钮。

:single-button – 类似于double-button伪类。对按钮来说，它用于判断一个按钮是否自己独立的在滚动条的一段。对内层轨道来说，它表示内层轨道是否紧靠一个single-button。

:no-button – 用于内层轨道，表示内层轨道是否要滚动到滚动条的终端，比如，滚动条两端没有按钮的时候。

:corner-present – 用于所有滚动条轨道，指示滚动条圆角是否显示。

:window-inactive – 用于所有的滚动条轨道，指示应用滚动条的某个页面容器（元素）是否当前被激活。（在webkit最近的版本中，该伪类也可以用于：：selection伪元素。webkit团队有计划扩展它并推动成为一个标准的伪类）。