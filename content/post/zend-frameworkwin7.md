---
title: 'zend framework环境配置（win7）'
date: 2011-09-09T01:50:00.000-07:00
draft: false
aliases: [ "/2011/09/zend-frameworkwin7.html" ]
tags : [zend framework, php]
---

确认的php和apache环境配置：
------------------

  
1.请PHPer确认你的PHP版本是否在5.2.0以上..如果不是的话..请更新到5.2.0,否则.Zend Framework 好像用不了。  

> PHP源码最新版下载地址为:[http://www.php.net/downloads.php.](http://www.php.net/downloads.php.)

  
2.你的PHP环境配置好了之后,请打开php.ini文件,确认PDO扩展是否打开：  

> ;extension=php\_mysql.dll  
> ;extension=php\_pdo.dll  
> ;extension=php\_pdo\_mysql.dll  
> 修改  
> extension=php\_mysql.dll  
> extension=php\_pdo.dll  
> extension=php\_pdo\_mysql.dll 去掉分号  
> 还有:  
> ; Windows: "path1;path2"  
> include\_path = ".;c:phpincludes;e:wwwrootzendflibrary" (zend framework的libary支持)

  
3.打开APACHE文件夹里面的httpd.conf文件.配置mod\_rewrite.so的支持  

> #LoadModule rewrite\_module modules/mod\_rewrite.so  
> 修改:LoadModule rewrite\_module modules/mod\_rewrite.so (去掉#号)才支持Zend framework

  
4\. 查找到httpd.conf文件,如果AllowOverride为None的话..请一定把None都改成all.这样你写.htaccess这样的文件才会起到作用..  

> 将所有的:AllowOverride None替换AllowOverride all 支持.htaccess文件

  
5.重新启动你的APACHE服务器.这样我们的PHP环境就可以运用Zend Framewrok了.  
  
   

安装zend tool
-----------

  
下载zend framework的zip文件，解压，配置环境变量的path路径：  

> 在path变量的结尾处增加：  
>   
> ;D:Program FilesZendZendFrameworkbin  
>   
> 该路径是解压目录中的bin目录的路径，注意前面的;不用丢掉

  
测试安装是否成功：  

> 在命令行中输入：  
>   
> zf show version  
>   
> 这句话是显示版本，如果顺利，会输出版本，否则会报错。我这里显示：  
>   
> zend framework version：1.11.10

  
   

创建项目
----

  
可以使用zendtool创建项目，命令行：  

> 通过命令行进入站点根目录（一般是htdocs目录），键入命令：  
>   
> zf create project zf\_proj  
>   
> 然后回车，会创建一个zf\_proj的目录，里面自动创建一些必要的文件和文件夹。目录结构如下：  
>   
> [![2011-09-09 16-39-23](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/09/2011-09-09-16-39-23_thumb.jpg "2011-09-09 16-39-23")](http://blogs8-wordpress.stor.sinaapp.com/uploads/2011/09/2011-09-09-16-39-23.jpg)  
>   
> 如果没有安装phpuint这个单元测试工具的话，会有提示，不过没关系，不用理它。

  
当然，你也可以把下载的目录拷贝到自建的目录，刚才那个命令并未做任何配置的工作。  
  
一般来说，图片、js和css文件要防止public目录下，而library文件夹里面要放下在到的zendframework的文件，就是把压缩包里的library文件夹里的内容（其实只有一个zend文件夹）拷贝过来就可以了。  
  
好了，现在我们测试下，打开以下地址：  

> http://localhost/zf\_proj/public/

  
就可以看到欢迎页面，这里就不上图了。