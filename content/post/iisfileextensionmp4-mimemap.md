---
title: 'IIS错误：在唯一密钥属性“fileExtension”设置为“.mp4”时，无法添加类型为
“mimeMap”的重复集合项'
date: 2011-10-18T07:18:00.000-07:00
draft: false
aliases: [ "/2011/10/iisfileextensionmp4-mimemap.html" ]
tags : [web, IIS]
---

> 之前，在一个站点的配置中增加了对mp4等文件类型的支持，重装系统后，发现站点的代码报错，错误内容即标题。详细情况如下

之前的IIS配置中，.mp4类型的文件默认无法访问，所以个人在IIS7.5的mime类型中增加了.mp4的类型的支持，但问题是需要每台机器中增加这些设置，不利于团队协作，于是，只好将这些修改体现在代码上，具体实现方式为：

在站点的web.config中增加了

```
    <staticContent>  
      <mimeMap fileExtension=".mp4" mimeType="video/mp4" />  
      <mimeMap fileExtension=".m4a" mimeType="video/mp4"/>  
      <mimeMap fileExtension=".m4v" mimeType="video/m4v" />  
      <mimeMap fileExtension=".ogv" mimeType="video/ogg"/>  
      <mimeMap fileExtension=".webm" mimeType="video/webm"/>  
    </staticContent>
```  
  

修改后一直正常运行。

  
  

前不久本人的电脑C盘空间不足，折腾多次后，无奈格盘重装，分给C盘很大的空间，系统也改为64位。今日重新运行该网站，即报错：

  
  

>   
> 
> 配置错误: 在唯一密钥属性“fileExtension”设置为“.mp4”时，无法添加类型为“mimeMap”的重复集合项。
> 
>   

  
  

怪的是代码现在仍在他人机器上正常运行，所以百度寻求答案。有云IIS应用程序池如果是集成模式，改为经典模式即可，但尝试后没有效果。

  
  

当然google也查了，这次甚为奇怪，google查不到（某些关键字又被屏蔽了？）。

  
  

这时候才想起来自己解决，看说明，似乎是扩展名的设置重复了嘛。于是删除该段配置，发现程序正常运行，且对于.mp4文件仍旧支持。

  
  

原来是IIS7.5默认的文件扩展支持增多了，而且，更可恶的是百度到的各种解释，竟然没有一个说这种解决方法的。

  
  

不过，为了与大家的一致，这里加了一段多余的配置：

  
  
```
    <staticContent>  
      <remove fileExtension=".mp4" />  
      <remove fileExtension=".m4a" />  
      <remove fileExtension=".m4v" />  
      <mimeMap fileExtension=".mp4" mimeType="video/mp4" />  
      <mimeMap fileExtension=".m4a" mimeType="video/mp4"/>  
      ……  
    </staticContent>
```