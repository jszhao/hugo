---
title: '[转载]Windows x64下配置ffmpeg的方法'
date: 2011-12-20T09:05:00.000-08:00
draft: false
aliases: [ "/2011/12/windows-x64ffmpeg.html" ]
tags : [转载, 资源]
---

**ffmpeg简介**  
FFmpeg 是一款跨平台的，对视频、音频进行录制、转换、播放的命令行形式软件，它使用的是 libavcodec 编解码器。FFmpeg 官方网站是 http://ffmpeg.org/ 。  
官方网站提供的是未编译的 FFmpeg 源代码。有兴趣的朋友可以自己下载源代码编译。一般我们都会偷懒,去下载现成的。  
**php\_ffmpeg资源**  
源码及编译方式见：

> [http://sergey89.ru/notes/ffmpeg-php-windows/](http://sergey89.ru/notes/ffmpeg-php-windows/)  

**各种win32版本的下载：**

> [http://ffmpeg.arrozcru.org/builds/](http://ffmpeg.arrozcru.org/builds/)

**配置方法**  
1，解压  
2，将 php\_ffmpeg.dll 文件 复制到 php 的 extension 目录  
3，将其他所有文件复制到 Windows 的SysWOW64目录(注意:32位系统复制到windows的 system32目录里)  
4，在 php.ini 文件中，增加一句： extension=php\_ffmpeg.dll  
5，重启 apache  
可以用 phpinfo(); 函数看安装结果。如果出现 ffmpeg support enable 字样，说明安装成功。  
刚开始的时候，把相关文件拷贝到了system32中，总出错，提示“unable to load dynamic library‘ xx/xx/php\_ffmpeg.dll’”，后来感觉可能是版本不对，想找到相应的64位的php\_ffmpeg.dll文件,走了很多弯路,最后看到转载的这篇文章，才成功。

       原创版本没有找到，网上传播很广的都是改过的转载，但链接都没有了。      我这里加入了两个链接，在此向原作者致谢，同时未经许可，对文字做了修改，请谅解。另外把我自己下载的版本提供给大家下载。我的是php5.3版本的：

[ffmpeg-php-5.3-win32-all.zip](http://dl.dbank.com/c0zc73aef3 "http://dl.dbank.com/c0zc73aef3")