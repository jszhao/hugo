---
title: '使用Facebook C# SDK开发facebook web app（入门及一些资源）'
date: 2011-08-18T23:15:00.000-07:00
draft: false
aliases: [ "/2011/08/facebook-c-sdkfacebook-web-app.html" ]
tags : [开放平台, facebook]
---

#### 就是样式有些难看，能不能再美一点？
[白兔]( "noreply@blogger.com") - <time datetime="2011-08-24T23:52:09.000-07:00">Aug 4, 2011</time>

就是样式有些难看，能不能再美一点？
<hr />
#### 自己创建的blog？很好很喜欢，加油喽！
[白兔]( "noreply@blogger.com") - <time datetime="2011-08-24T23:52:09.001-07:00">Aug 4, 2011</time>

自己创建的blog？很好很喜欢，加油喽！
<hr />
#### 还不太会调整，在学习，多谢关注
[Jason.Z](http://blogs8.sinaapp.com "noreply@blogger.com") - <time datetime="2011-08-25T23:20:20.000-07:00">Aug 5, 2011</time>

还不太会调整，在学习，多谢关注
<hr />
#### 请问ＦＡＣＥＢＯＯＫ的集成require 'src/facebook.php';
[kivu]( "noreply@blogger.com") - <time datetime="2011-10-19T04:41:08.000-07:00">Oct 3, 2011</time>

请问ＦＡＣＥＢＯＯＫ的集成require 'src/facebook.php';  
  
// Create our Application instance (replace this with your appId and secret).  
$facebook = new Facebook(array(  
'appId' => '175974682484604',  
'secret' => '2cc482fd8cb824b1b95a3afe51b50aac',  
));设置已经正确了，为什么不可以用呢，是不是还要其它条件设置？谢谢！
<hr />
#### 你说的是php的sdk么？
[Jason.Z](http://blogs8.sinaapp.com "noreply@blogger.com") - <time datetime="2011-10-19T04:45:50.000-07:00">Oct 3, 2011</time>

你说的是php的sdk么？
<hr />
#### [@kivu](#comment-6)
[Jason.Z](http://blogs8.sinaapp.com "noreply@blogger.com") - <time datetime="2011-10-19T04:54:55.000-07:00">Oct 3, 2011</time>

[@kivu](#comment-6)  
facebook 官网的文档中，初始化确实这一段就可以了：  
  
require\_once("facebook.php");  
$config = array();  
$config\[‘appId’\] = 'YOUR\_APP\_ID';  
$config\[‘secret’\] = 'YOUR\_APP\_SECRET';  
$config\[‘fileUpload’\] = false; // optional  
$facebook = new Facebook($config);  
  
你说不可以的是什么情况？
<hr />
#### 我刚才下载了最近版的facebook，PHP sdk，测试过程中也遇到问题了，提示curl执行失败。...
[Jason.Z](http://blogs8.sinaapp.com "noreply@blogger.com") - <time datetime="2011-10-19T07:59:24.000-07:00">Oct 3, 2011</time>

我刚才下载了最近版的facebook，PHP sdk，测试过程中也遇到问题了，提示curl执行失败。  
就是说，首先需要启用php.ini中的extension=php\_curl.dll扩展；  
其次，在访问facebook某些api的时候，一些访问路径被GFW给墙了，如果要做测试，最好使用VPN。  
另外，似乎sdk中的key和安全码已经失效了，需要自己再申请一个帐号，创建应用，并设置应用可以通过url访问，然后把key和安全码换成自己的，否则会在链接登录到facebook时报一个应用错误的提示。  
不知道你遇到的是否如此。欢迎来信交流。
<hr />
#### [@Jason.Z](#comment-9) ...
[kivu23]( "noreply@blogger.com") - <time datetime="2011-10-20T15:14:59.000-07:00">Oct 5, 2011</time>

[@Jason.Z](#comment-9)  
  
php sdk后面我知道为什么失效了，要先给当前用户LOGIN，允使操作这个应用程序。  
现在我就有点想不明白，这个应用程序本想让浏览客户看到PHP SDK调出来数据显示在专题的，如果不"LOGIN,允使操作这个应用程序",$facebook->getUser()反回是null。
<hr />
#### 这是因为demo中的登录过程，包含了部分app授权的过程。我记得在js版的facebook登录的方法...
[Jason.Z](http://blogs8.sinaapp.com "noreply@blogger.com") - <time datetime="2011-10-24T03:19:40.000-07:00">Oct 1, 2011</time>

这是因为demo中的登录过程，包含了部分app授权的过程。我记得在js版的facebook登录的方法中，可以直接写进入访问权限的参数，代码大概是这样的  
FB.login(handleSessionResponse, {  
perms: 'user\_videos,publish\_stream'  
});  
我记得这样写之后，从facebook直接点开该app时，会自动授权，不需要重新登录。  
你的链接我看到了，我也研究下。
<hr />
