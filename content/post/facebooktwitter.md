---
title: '有关调用facebook和twitter发布消息的研究结果记录'
date: 2011-07-31T23:21:00.000-07:00
draft: false
aliases: [ "/2011/07/facebooktwitter.html" ]
tags : [开放平台, twitter, api, facebook]
---

         近期有个mobile的项目，需要在项目中加两个功能，是在twitter和facebook上发消息的按钮，客户不想自己做界面，希望直接调用各自网站上的页面发消息。这俩网站国外很流行，国内却无法访问，也没怎么用过，不甚了了。于是开始查资料。

1、Twitter

          twitter上发消息其实很简单，很多地方提供生产twitter按钮的功能，访问也完全支持get方式的访问。就是说完全可以直接输入一个url地址，就可以打开对应的twitter页面，且可以传入想要的信息内容。对比国外网站对twitter的引用和官网上提供的button按钮的说明，我们很快找到了这样一个地址，示例如下：

         [http://twitter.com/intent/tweet?status=i%20think%20is%20 @zsz417 http://baidu.com](http://twitter.com/intent/tweet?status=i%20think%20is%20 @zsz417 http://baidu.com)

        这个地址用来修改用户的状态（status），不过twitter似乎也就有个状态而已，所有的信息无非就是通过status来表现的。你看，有内容，有url，还可以@某人，直接写入，就很完整了啊。当然，由于twitter中对状态有140个字符数的限制（国外的140个字符能表达充足的含义么），如果url比较长，最好通过短网址服务把网址做短喽。经过简单的几个测试后，就把该链接交给mobile开发的同事去用。

         不过，很快mobile组那边就找过来，说每次发twitter都得登录，那是相当的麻烦。经跟踪发现，用mobile访问twitter，它会自动重定向到mobile.twitter.com进行登录，而且cookie仅仅对mobile.twitter.com有效（当然对当前连接也是有效的），而这个网址是web版本的，它需要[www.twitter.com](http://www.twitter.com ) 这个网址上的cookie。（这个twitter也太不地道了，咋能这样呢？）

         由于英语欠佳，资料查起来也很费力，而且baidu上面的资料几乎没有有效的，而google搜索相关敏感词语时，时不时就被墙了。喵了个咪的，人在天朝，全凭想象啊，猜吧。改下访问路径：

         [http://mobile.twitter.com/intent/tweet?status=i%20think%20is%20 @zsz417 http://baidu.com](http://mobile.twitter.com/intent/tweet?status=i%20think%20is%20 @zsz417 http://baidu.com)

        键入地址后，竟然跳到了@intent这个用户的页面，我了个去，还是加密连接。看来这次猜的不给力啊。

        边查边猜，发现另外一种写法：

        [http://twitter.com/?status=内容](http://twitter.com/?status=内容)

        那么，是不是它有对应的mobile版的写法呢：

        [https://mobile.twitter.com/?status=](https://mobile.twitter.com/?status=)

       这个加密连接，是自动重定向上去的（我修改agent为iphone了）。那在mobile 上直接访问这个，岂不更好？急忙交割mobile的同事去测试，OK，通过了。

2、Facebook

       facebook的资料还是有一些的，比较适合我们的，资料上有这样一个链接（注意这个用法已过期）：

       [http://www.facebook.com/sharer/sharer.php?u=http://www.sina.com.cn&t=this is sina](http://www.facebook.com/sharer/sharer.php?u=http://www.sina.com.cn&t=this is sina)

      其中参数u是链接，t是标题，意味着可以分享一个网页，并且可以定义title。09年很多网页中也都是这么写的。但经过测试，facebook对这个sharer进行了优化，它自动抓取了网页的title以及首页的title，甚至抓取了其中的某些图片和描述信息，另外还有个用于用户输入的框。也就是说，现在这个只有u参数的写法还是对的，t这个参数已经无效了。几经测试，并不能完成客户的需求，只好再查找其他资源。（我并没有从facebook api文档中找到这个sharer的用法，惭愧）。

     在facebook api文档的对话框项目中，发现有这么个用法：

     [http://www.facebook.com/dialog/feed?app\_id=123050457758183&redirect\_uri=http://www.facebook.com&message=I%27m%20not%20ignoring%20you.%20I%27m%20just%20waiting%20for%20you%20to%20talk%20to%20me%20first.%20http://baidu.com](http://www.facebook.com/dialog/feed?app_id=123050457758183&redirect_uri=http://www.facebook.com&message=I%27m%20not%20ignoring%20you.%20I%27m%20just%20waiting%20for%20you%20to%20talk%20to%20me%20first.%20http://baidu.com "http://www.facebook.com/dialog/feed?app_id=123050457758183&redirect_uri=http://www.facebook.com&message=I%27m%20not%20ignoring%20you.%20I%27m%20just%20waiting%20for%20you%20to%20talk%20to%20me%20first.%20http://baidu.com")

     其中：

1.      app\_id是必填，且必须是1230204577581这么个值，这个值是facebook的一个应用（app）的id，叫做Cool Social App，我想这算是facebook提供的公用app吧；
2.      redirect\_uri也是必填参数，在用户发布涂鸦墙消息后，返回到哪个页面。我们这个应用并没有自己的独立页面，所以返回到facebook上吧；
3.      message表明你要给涂鸦墙的输入框中带过去的内容文字，没有数量限制，当然，这个不是必填项目。

    其他的非必填项有：

1.  link，链接地址；
2.  picture，可以显示一个图片；
3.  name，链接的title，显示在涂鸦墙输入框的下面第一行，字体是加粗的；
4.  caption，说明/摘要，显示在标题下面一行；
5.  description,详细介绍，显示在摘要的下面。

   先留下来做记录吧，不知道facebook哪天又要修改它的api，/晕。

   备注，注意翻 墙。