---
title: 'Aptana集成jquery及中文包的问题'
date: 2011-10-18T11:25:00.000-07:00
draft: false
aliases: [ "/2011/10/aptanajquery.html" ]
tags : [资源]
---

> 话说，最早使用Aptana的时候，还是06年。此后在各种Web的开发场合，一直大力宣传推广该软件的使用。Aptana经历一段时间的收费风波后，现在又开始免费使用了，于是搞了个最近的版本装上，却发现英文的还是不习惯啊，整了个中文包，记录下。

Aptana是一个基于Eclipse的集成开发环境，其最广为人知的是它非常强悍的JavaScript编辑器和调试器。近年来又扩展了Ruby和iphone的集成开发环境，最新版的还支持jquery和其他一些js框架，可以依据jquery的文档，直接在编辑器中对方法给予智能提示，这对我们这些习惯了vs开发的人来说，太有利了。

支持jquery看来很简单，这里有一段拷贝来的说明，大家看看吧，今天没时间测试了，明天再测试：

```
安装完毕Aptana后，还不能使用智能提示的，应为没有库，我们还要在我们的项目中加入库文件。  
  
首先下载：[https://raw.github.com/aptana/javascript-jquery.ruble/master/support/jquery.1.4.2.sdocml](https://raw.github.com/aptana/javascript-jquery.ruble/master/support/jquery.1.4.2.sdocml) 一定 要保证是.sdocml文件不是文本文档  
  
再下载：http://code.jquery.com/jquery-1.4.1-vsdoc.js或是[http://encosia.com/source/jquery/jquery-1.5.0-vsdoc.js](http://encosia.com/source/jquery/jquery-1.5.0-vsdoc.js)（需要3.0.1版本以上）
```  
  

至于中文包，其实跟所有基于Eclipse的集成开发环境一样，都要找这个地址去安装，zend啊什么的，都要照这个路径安装就行，不过汉化的不全。

  
  

路径是：

  
  

>   
> 
> [http://download.eclipse.org/technology/babel/update-site/R0.9.0/helios/](http://download.eclipse.org/technology/babel/update-site/R0.9.0/helios/ "http://download.eclipse.org/technology/babel/update-site/R0.9.0/helios/")
> 
>   

  
  

至于怎么安装，不用说了吧。