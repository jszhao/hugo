---
title: 'Amazon ec2的ubuntu中设置apache支持H264流式媒体'
date: 2012-03-17T10:19:00.000-07:00
draft: false
aliases: [ "/2012/03/amazon-ec2ubuntuapacheh264.html" ]
tags : [资源, php]
---

> 做个视频网站，但并没有使用专门的流媒体服务器，所以要在apache上开个流式媒体输出。

  
这里的流式媒体输出跟专业的流媒体服务是两回事。这是在apache中添加一个模块，使得以视频流的方式访问，并不需要全部加载完视频后就可以播放。  
  
这里只支持h264编码的mp4后缀。所需要启用的模块为：  
  
mod\_h264\_streaming  

安装过程：
=====

  

1、首先安装 apxs2
------------

  
sudo apt-get install apache2-threaded-dev  

2、下载 H264 Streaming Module for Apache
-------------------------------------

  
cd ~  
wget http://h264.code-shop.com/download/apache\_mod\_h264\_streaming-2.2.7.tar.gz  
tar -zxvf apache\_mod\_h264\_streaming-2.2.7.tar.gz  

3、编译
----

  
cd ~/mod\_h264\_streaming-2.2.7  
./configure –with-apxs=\`which apxs2\`  
make  
sudo make install  
注意：ubuntu 的apxs2 位于 /usr/bin/apxs2  

4、编辑 Apache 的配置文件 （/etc/apache/httpd.conf）以添加流媒体处理选项
----------------------------------------------------

  
LoadModule h264\_streaming\_module /usr/lib/apache2/modules/mod\_h264\_streaming.so  
AddHandler h264-streaming.extensions .mp4  

5、最后，重启 Apache 即可
-----------------

  
sudo /etc/init.d/apache2  restart  

实际应用中遇到的问题
==========

  
由于有些缩略图的文件名中包含”.mp4.”这样的部分，路径中出现这样的部分同样会被该模块捕获，但又处理不了，所以会报错。为此我们专门修改了文件的路径。