---
title: 'PHP Document 注释标记及规范(转载)'
date: 2011-12-27T04:25:00.000-08:00
draft: false
aliases: [ "/2011/12/php-document.html" ]
tags : [php]
---

**@access**  
使用范围：class,function,var,define,module  
该标记用于指明关键字的存取权限：private、public或proteced

**@author**  
指明作者

**@copyright**  
使用范围：class，function，var，define，module，use  
指明版权信息  
**@deprecated**  
使用范围：class，function，var，define，module，constent，global，include  
指明不用或者废弃的关键字

**@example**  
该标记用于解析一段文件内容，并将他们高亮显示。Phpdoc会试图从该标记给的文件路径中读取文件内容

**@const**  
使用范围：define  
用来指明php中define的常量

**@final**  
使用范围：class,function,var  
指明关键字是一个最终的类、方法、属性，禁止派生、修改。

**@filesource**  
和example类似，只不过该标记将直接读取当前解析的php文件的内容并显示。

**@global**  
指明在此函数中引用的全局变量

**@ingore**  
用于在文档中忽略指定的关键字

**@license**  
相当于html标签中的<a>,首先是URL，接着是要显示的内容  
例如<a href=”http://www.baidu.com”>百度</a>  
可以写作 @license http://www.baidu.com 百度

**@link**  
类似于license  
但还可以通过link指到文档中的任何一个关键字

**@name**  
为关键字指定一个别名。

**@package**  
使用范围：页面级别的-> define，function，include  
类级别的->class，var，methods  
用于逻辑上将一个或几个关键字分到一组。

**@abstrcut**  
说明当前类是一个抽象类

**@param**  
指明一个函数的参数

**@return**  
指明一个方法或函数的返回指

**@static**  
指明关建字是静态的。

**@var**  
指明变量类型

**@version**  
指明版本信息

**@todo**  
指明应该改进或没有实现的地方

**@throws**  
指明此函数可能抛出的错误异常,极其发生的情况

普通的文档标记标记必须在每行的开头以@标记，除此之外，还有一种标记叫做inline tag,用{@}表示，具体包括以下几种：

**{@link}**  
用法同@link

**{@source}**  
显示一段函数或方法的内容

##### 注释规范

a.注释必须是

> /\*\*  
> \* 注释内容  
> \*/

的形式

b.对于引用了全局变量的函数，必须使用glboal标记。

c.对于变量，必须用var标记其类型（int,string,bool…）

d.函数必须通过param和return标记指明其参数和返回值

e.对于出现两次或两次以上的关键字，要通过ingore忽略掉多余的，只保留一个即可

f.调用了其他函数或类的地方，要使用link或其他标记链接到相应的部分，便于文档的阅读。

g.必要的地方使用非文档性注释，提高代码易读性。

h.描述性内容尽量简明扼要，尽可能使用短语而非句子。

i.全局变量，静态变量和常量必须用相应标记说明

```
<?php  
/\*\*  
\* Sample File 2, phpDocumentor Quickstart  
\*  
\* This file demonstrates the rich information that can be included in  
\* in-code documentation through DocBlocks and tags.  
\* @author Greg Beaver <cellog@php.net>  
\* @version 1.0  
\* @package sample  
\*/  
   
//PHP code  
   
/\*\*  
\* A sample function docblock  
\* @global string document the fact that this function uses $\_myvar  
\* @staticvar integer $staticvar this is actually what is returned  
\* @param string $param1 name to declare  
\* @param string $param2 value of the name  
\* @return integer  
\*/  
function firstFunc($param1, $param2 = 'optional') {  
    static $staticvar = 7;  
    global $\_myvar;  
    return $staticvar;  
}
```