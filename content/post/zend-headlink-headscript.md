---
title: '使用Zend headLink 和headScript'
date: 2011-11-22T10:48:00.000-08:00
draft: false
aliases: [ "/2011/11/zend-headlink-headscript.html" ]
tags : [开发随笔, zend framework, php]
---

> 今天用到了一个新的小功能，是关于headLink和headScript的，对于新手来说需要学习的东西很多啊。经查资料找到用法，这里记录下：

headLink用于对css文件的引用，而headScript用于js脚本文件的引用。可以直接在模板中使用，代码如下：

```
/application/views/layouts/layout.phtml  
    headLink()->appendStylesheet($this->baseUrl() . '/css/global.css')  
        ->headLink()->prependStylesheet($this->baseUrl() . '/css/resets.css')  
    ?>  
headLink() ?>
```  
  

  
  

也可以在代码中添加，比如这里在Bootstrap.php中进行了添加：

  
  
```
/application/Bootstrap.php (function not complete)  
 function \_initView()  
 {  
  $view->headLink()->prependStylesheet('css/resets.css')  
   ->headLink()->appendStylesheet('css/global.css')  
   ->headLink()->appendStylesheet('css/forms.css')  
   ->headLink()->appendStylesheet('css/pages.css');  
 }
```  
  

  
  
```
 headScript使用方法一致，不过方法名改了，代码如下：
```  
  
```
/application/Bootstrap.php (function not complete)  
 function \_initView()  
 {  
  $view->headScript()->prependFile('js/jquery-1.3.2.min.js')  
   ->headScript()->appendFile('js/jquery-ui-1.7.min.js')  
   ->headScript()->appendFile('js/jquery.easing.1.3.js')  
   ->headScript()->appendFile('js/docready.js');   
 }
```  
  

我们举的例子都是在Bootstrap.php中使用的例子，再举个控制器中使用的例子：

  
  
```
public function init ()  
{  
	$this->\_helper->layout()->setLayout("default");  
	$this->view->headLink()->appendStylesheet('/public/css/commshot.css');  
}
```  
  

  
  

这个init是控制器的初始化方法。