---
title: 'Amazon Ec2的ubuntu登录问题Permission denied (publickey)'
date: 2012-03-17T10:07:00.000-07:00
draft: false
aliases: [ "/2012/03/amazon-ec2ubuntupermission-denied.html" ]
tags : [cloud, 开放平台, 资源]
---

> 前几天开了个Amazon aws的帐号，由于是第一次碰，一点点摸索着运行了一台Ubuntu 10.04的实例，遇到的问题记录下。

  

1、记得开放需要的端口
===========

  
linux一般使用SSH客户端进行连接，所以必须要打开22端口，否则实例化后连不上，等于白白消耗你的信用卡。  
  
一般启动一个LAMP的话，这几个端口是需要打开的：  

  
*   21：ftp
  
*   22：ssh
  
*   80：http
  
*   mysql:3306
  

  
当然这些端口都可以修改的。但第一次一定要开的就是ssh的端口。  

2、记得上网搜索问题,我遇到问题是Permission denied (publickey)
==============================================

  
Amazon的帮助文档还是比较全面的，我这个不懂linux的人，也可以参照帮助文档连接，不过，文档中可是有错误的。比如点击连接计算机的时候，这里就只是说输入这样的命令就可以进去：  
  
ssh -i xxx.pem [root@your](mailto:root@your) Public DNS  
  
可是总也连不上去，提示：  

> Permission denied (publickey)

  
后来查了下，说linux第一次进去时，root是没启用的，要用ec2-user这样一个用户名。无奈，尝试了，还是不行，进不去啊。（后来查的说是在ami上自己安装的ubuntu使用ec2-user，我们是直接实例化的系统，不一样）  
  
直到最后才搜索到ubuntu的登录又得用另外一个用户名：ubuntu。  
  
其他关于ubuntu的设置，就不记录了，网上的资料比较多。