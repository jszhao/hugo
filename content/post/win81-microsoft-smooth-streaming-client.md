---
title: 'win8.1 Microsoft Smooth Streaming Client'
date: 2014-04-18T01:23:00.001-07:00
draft: false
aliases: [ "/2014/04/win81-microsoft-smooth-streaming-client.html" ]
---

做视频流的都知道苹果的hls视频流格式，其实微软也有一种流格式叫做Smooth Streaming。

hls在苹果的设备上可是默认支持的，不用安装什么解码器，直接浏览器打开就能播放，据说人家那是硬件解码，别的设备就得自己想办法了。

Smooth Streaming就没这么好运气了，即便在微软的windows8.1的操作系统中，微软也没能提供web版的Smooth Streaming播放器。只是win8 store App倒是提供了Smooth Streaming Client 的SDK。

今天我们讨论的重点就是这个播放器的sdk。

首先是播放器的主体：Playerframework：[http://playerframework.codeplex.com/](http://playerframework.codeplex.com/)

这个播放器还算正常，但是怎么说呢。如果你是WinJS开发的App，总感觉多此一举，毕竟html5自带的播放器就可以播放视频，通过js扩展出的各种漂亮的播放器，都不比微软的差。

这个播放器呢，使用还算简单，不多说了。我们说他的Smooth Streaming Client SDK

这个sdk有两个版本：[win8.1版](http://visualstudiogallery.msdn.microsoft.com/0170c67c-c183-4fee-8dd4-c2b44d710d40) 和 [Win8版](http://visualstudiogallery.msdn.microsoft.com/04423d13-3b3e-4741-a01c-1ae29e84fea6)

好吧，我一直以为win8.1是win8的一个更新呢，原来可以认为是两个东西哦。

这里有一个关于html app中使用这东西的[文档](http://playerframework.codeplex.com/wikipage?title=Windows%208%20Player%3a%20Install%20and%20configure%20-%20HTML%2fJavaScript&referringTitle=Windows%208%20Player%20Documentation)，不翻译了。