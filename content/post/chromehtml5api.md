---
title: '记录一个chrome的HTML5离线存储api地址'
date: 2013-02-15T19:12:00.001-08:00
draft: false
aliases: [ "/2013/02/chromehtml5api.html" ]
---

这是未来会支持的chrome浏览器的离线存储api，叫SyncFileSystem API，现在正式版的chrome浏览器尚不支持，该api支持云端同步功能，同步到google网盘，不过，该api现在只是处于草稿状态，这里记录下地址。

[https://sites.google.com/a/chromium.org/dev/developers/design-documents/extensions/proposed-changes/apis-under-development/syncfilesystem-api](https://sites.google.com/a/chromium.org/dev/developers/design-documents/extensions/proposed-changes/apis-under-development/syncfilesystem-api)

**原创文章，转载请注明：** 转载自[Happiness space](https://byszsz.com/)

**本文链接地址:** [记录一个chrome的HTML5离线存储api地址](https://byszsz.com/archives/495.htm)