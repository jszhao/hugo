---
title: 'PHP获取JSON数据-关于zencoder中的notification'
date: 2012-04-28T21:49:00.001-07:00
draft: false
aliases: [ "/2012/04/phpjson-zencodernotification.html" ]
---

> 之前做的项目，json都是格式化成字符串传到后台，然后使用字符串转换成json的方法把json字符串转换为对象，此次使用Zencoder的API，处理完成后对方会提交一个notification到我们的地址，此时在request中无法获得所需的内容，所以获得的提醒内容总是空的。

其实，最初在写这些方法时，曾经自己做过实验，使用jquery中ajax的json方式post的数据，但结果非常简单，jquery直接把post json的请求转为类似url的get请求方式，以至于在参数不太多的情况下，都可以直接按照get url方式来模拟该请求，也就是说json并没有以对象方式post回服务器，而是仍然以字符串post的。

最初我是参照我自己的实验对notification进行了处理，正式测试中却发现Zencoder的提醒没有被获取的，那么他是以什么方式post这些数据的呢？

经尝试，$\_post和$\_REQUEST都获取不到。查找资料后发现，PHP默认只识别application/x-www.form-urlencoded标准的数据类型，因此，对型如text/xml 或者 soap 或者 application/octet-stream 之类的内容无法解析，如果用$\_POST数组来接收就会失败！那怎么办呢？

网上说可以用它来获取：$GLOBALS\['HTTP\_RAW\_POST\_DATA'\] ，不过似乎比较麻烦，当然，还有一个简单的办法： php://input 。

php://input 允许读取 POST 的原始数据。和 $HTTP\_RAW\_POST\_DATA 比起来，它给内存带来的压力较小，并且不需要任何特殊的 php.ini 设置。php://input 不能用于 enctype=”multipart/form-data”。

使用起来很简单：

> file\_get\_contents(“php://input”);

**原创文章，转载请注明：** 转载自[Happiness space|Jason and chris](https://byszsz.com/)

**本文链接地址:** [PHP获取JSON数据-关于zencoder中的notification](https://byszsz.com/archives/453.htm)