---
title: '土豆登录认证api地址'
date: 2011-09-28T04:28:00.000-07:00
draft: false
aliases: [ "/2011/09/api.html" ]
tags : [开放平台, api]
---

> 土豆的api很有问题，其登录使用了OAuth的认证，相关说明却直接链接到OAuth的官网去了，那总得把自己的认证服务的路径写上去吧！

找了半天，终于从其论坛中找到以下几个地址，

> ##### Tudou OAuth服务URL
> 
>   
> Request Token URL: 获取未授权的Request Token服务地址；  
> [http://api.tudou.com/auth/request\_token.oauth](http://api.tudou.com/auth/request_token.oauth)  
> User Authorization URL: 获取用户授权的Request Token服务地址；  
> [http://api.tudou.com/auth/authorize.oauth](http://api.tudou.com/auth/authorize.oauth)  
> Access Token URL: 用授权的Request Token换取Access Token的服务地址；  
> [http://api.tudou.com/auth/access\_token.oauth](http://api.tudou.com/auth/access_token.oauth)

论坛说文档中增加了，其实文档中还是没有。先记下，免得忘记。