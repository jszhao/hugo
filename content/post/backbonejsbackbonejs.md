---
title: 'Backbonejs集合的排序（正序/逆序）—–Backbonejs学习笔记'
date: 2013-09-16T00:43:00.001-07:00
draft: false
aliases: [ "/2013/09/backbonejsbackbonejs.html" ]
---

网页开发中经常遇到js排序的问题，这曾经是前端工程师最头疼的问题。不过使用Backbonejs的同学，还是有不少捷径可走的。 1、sortBy方法： `var sortcollection=this.collection.sortBy(function(music){ return music.get("title").toLowerCase(); }); this.collection.reset(sortcollection);` 直接sortBy并不能重新排序，必须手动触发reset方法。 2、comparator： `this.collection.comparator = function(music) { return music.get("id");// }; 自动排序，号称每增加一个会自动排到正确的位置。不过我没有试验成功。` 3、逆序reverse 话说reverse方法实际上是js数组标准的方法，在这里使用起来仍然很方便。 `var sortcollection=this.collection.sortBy(function(music){ return music.get("title").toLowerCase(); }).reverse(); this.collection.reset(sortcollection);` 之所以一开始并没有想到可以直接用，是因为collection在跟踪模式下显示并不是一个数组而是一个对象。   另外，在使用中发现，collection没有提供清空方法，而且reset时，页面已有的元素并没有被排序，而是重新渲染了一批新的经过排序的内容进来。我理解这是因为集合数据并不跟html的内容同步的原因，处理也蛮简单的，我们直接清空容器dom的html就可以了。

  
  
[Backbonejs集合的排序（正序/逆序）—–Backbonejs学习笔记](https://byszsz.com/archives/601.htm?utm_source=rss&utm_medium=rss&utm_campaign=backbonejs%25e9%259b%2586%25e5%2590%2588%25e7%259a%2584%25e6%258e%2592%25e5%25ba%258f%25ef%25bc%2588%25e6%25ad%25a3%25e5%25ba%258f%25e9%2580%2586%25e5%25ba%258f%25ef%25bc%2589-backbonejs%25e5%25ad%25a6%25e4%25b9%25a0%25e7%25ac%2594%25e8%25ae%25b0)