---
title: '介绍几个移动web开发调试工具'
date: 2014-04-18T01:24:00.001-07:00
draft: false
aliases: [ "/2014/04/web.html" ]
---

<p>移动web开发，最让人心烦的估计就是各平台下运行结果不一致的问题了，明明在电脑上调试的好好的，在手机上却错误，而且错的莫名其妙。咋办？

1、<a href="http://jsconsole.com/">http://jsconsole.com/</a>

最初，大家期望都不高，能把console.log输出出来就好了，当然，高版本iphone在safari中内置这个功能了，但其他手机呢？

这时候我们可以求助jsconsole.com，他其实提供了一个在线的服务，帮你来完成console.log的输出。你需要改什么？什么都不需要改。打开网页看看吧。

不多说，输入<span style="font-family: Menlo, consolas, monospace; font-size: 15px; line-height: 20px; white-space: pre-wrap;">:listen,然后就可以得到一个脚本地址的标签，把该标签贴进你的网页里面即可。</span>

<span style="font-family: Menlo, consolas, monospace; font-size: 15px; line-height: 20px; white-space: pre-wrap;">原理其实就是把页面中控制台输出的内容显示打你当前的网页上了，不错吧。</span>

<span style="font-family: Menlo, consolas, monospace; font-size: 15px; line-height: 20px; white-space: pre-wrap;">2、weinre</span>

<span style="font-family: Menlo, consolas, monospace;"><span style="font-size: 15px; line-height: 20px; white-space: pre-wrap;">有了输出结果，我们其实更不满足了，我还想想在pc上的浏览器那样，看dom的情况嘞，这个咋办？</span></span>

<span style="font-family: Menlo, consolas, monospace;"><span style="font-size: 15px; line-height: 20px; white-space: pre-wrap;">我们知道chrome等webkit浏览器底下有个开发工具，可以查看dom情况，脚本执行情况，比单纯的console.log输出强多了,得此工具，可以得天下乎。</span></span>

<span style="font-family: Menlo, consolas, monospace;"><span style="font-size: 15px; line-height: 20px; white-space: pre-wrap;">weinre就是这样一个强大的工具，原理是：在安装一个weinre的服务，其实就是一个提供api的网站，然后跟jsconsole中一样，把生成的一段代码加入到你的网页中去（也可以不加入，用收藏夹方式调试也可以）。然后这段脚本就把页面中所有的信息（webkit的）发到你的服务器，然后展示给你。这个脚本依赖webkit，所以现在支持webkit的浏览器。不过现在主流的移动端浏览器都是webkit的，ie和firefox嘛，还只占很小比例。</span></span>

<span style="font-family: Menlo, consolas, monospace;"><span style="font-size: 15px; line-height: 20px; white-space: pre-wrap;">weinre据说早期是java开发的，现在有nodejs版，使用可以参考以下文章：</span></span>

<a href="http://ju.outofmemory.cn/entry/1355">http://ju.outofmemory.cn/entry/1355</a>

（经测试，在chrome浏览器上正常，而ios7的safari中与某些脚本有冲突，会报错，不知道是不是nodejs版的weinre还没正式完成？）

3、firebug-lite

前面说到，weinre只支持webkit浏览器，其他浏览器我们咋处理哪？

做web开发的，一定想到firebug那近乎完美个的调试工具，想来做web开发这几年基本上没有离开过firebug，重装电脑第一个要装的浏览器是firefox，第一个要装的插件就是firebug啦。

不过之前一直没用过，把firebug-lite弄到手机浏览器上去。

其实很简单啦，跟前面的一个，把一段脚本嵌入到你的页面去就好了。

参考：<a href="https://getfirebug.com/firebuglite">https://getfirebug.com/firebuglite</a>

不过firebug这么强大的东东，在小小的浏览器窗口中施展的着实不甚自在。要是集weinre和firebug的种种优点于一身，哈哈。</p>